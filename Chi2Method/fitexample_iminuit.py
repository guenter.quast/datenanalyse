''' general example for fitting with class mnFit from PhyPraKit.phyFit
      - read datafile fitexample.dat
      - perform fit (2nd order polynomial)
      - show output
'''
# Imports  #
from PhyPraKit.phyFit import mnFit
import numpy as np, matplotlib.pyplot as plt

# fit function definition 
def poly2(x, a=1.0, b=0.0, c=0.0):
    return a * x**2 + b * x + c

# set fit function 
fitf=poly2             # own definition    

# Workflow #
# 1. load data 
x, y, sy = np.loadtxt("fitexample.dat", unpack=True)
# 2. set up Fit object
myFit=mnFit()
# 3. initialize data object
myFit.init_data(x, y, ey=sy, erelx=0.05)
# 4. initalize fit object
myFit.init_fit(fitf)
# 5. perform fit
myFit.do_fit()
# 6. retrieve results
FitResult = myFit.getResult()
# 7.make result plot
fig=myFit.plotModel()
# 8.print results
np.set_printoptions(precision=3)
print("*==* Result of fit:\n", FitResult)
# 9. finally, show plot
plt.show()
