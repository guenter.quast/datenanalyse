''' general example for fitting with kafe
      - read datafile fitexample.dat
      - perform fit (2nd order polynomial)
      - show and save output
'''
# Imports  #
############ import everything we need from kafe
from kafe import *
from kafe.function_tools import FitFunction, LaTeX, ASCII
from kafe.function_library import quadratic_3par

# fit function definition with decorators for nice output
@ASCII(expression='a * x^2 + b * x + c')
@LaTeX(name='f', parameter_names=('a', 'b', 'c'), expression='a\\,x^2+b\\,x+c')
@FitFunction
def poly2(x, a=1.0, b=0.0, c=0.0):
    return a * x**2 + b * x + c

# set fit function 
fitf=poly2             # own definition    
#fitf=quadratic_3par   # from kafe function library

# Workflow #
############# load the experimental data from a file
my_dataset = parse_column_data(
    'fitexample.dat',
    field_order='x,y,yabserr',
    title='example data')
# Create the Fit
my_fit = Fit(my_dataset, fitf)
# Do the Fits
my_fit.do_fit()
# Create the plots
my_plot = Plot(my_fit)
# Set the axis labels
my_plot.axis_labels = ['$x$', 'data \& $f(x)$']
# Draw the plots
my_plot.plot_all()

# Plot output #
###############
# Save the plots
my_plot.save('kafe_fitexample.pdf')
# Show the plots
my_plot.show()
