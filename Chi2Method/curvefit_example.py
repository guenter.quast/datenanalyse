import numpy as np
from scipy.optimize import curve_fit

# fit function definition 
def poly2(x, a=1.0, b=0.0, c=0.0):
    return a * x**2 + b * x + c

#1. load data
x, y, sy = np.loadtxt("fitexample.dat", unpack=True)
# linear least squares with scipy.optimize.curve_fit
par, cov = curve_fit( poly2, x, y, sigma=sy, absolute_sigma=True )
print("Fit parameters:\n", par)
print("Covariance matrix:\n", cov)


