''' general example for fitting with kafe2
      - read datafile fitexample.dat
      - perform fit (2nd order polynomial)
      - show and save output
'''
# Imports  #
from kafe2 import XYContainer, Fit, Plot
import numpy as np, matplotlib.pyplot as plt

### define the model function 
def poly2(x, a=1.0, b=0.0, c=0.0):
    return a * x**2 + b * x + c

# Workflow #

# 1. load the experimental data from a file
x, y, e = np.loadtxt('fitexample.dat', unpack=True)

# 2. convert to kafe2 data structure and add uncertainties
xy_data = XYContainer(x, y)
xy_data.add_error('y', e)                    # independent erros y
xy_data.add_error('x', 0.05, relative=True)  # independent relative errors x
# set meaningful names 
xy_data.label = 'Beispieldaten'
xy_data.axis_labels = ['x', 'data & f(x)']

# 3. create the Fit object 
my_fit = Fit(xy_data, poly2)
# set meaningful names for model
my_fit.model_label = 'Parabel-Modell'

# 4. perform the fit
my_fit.do_fit()

# 5. report fit results
my_fit.report()

# 6. create and draw plots
my_plot = Plot(my_fit)
my_plot.plot()

# 7. show or save plots #
## plt.savefig('kafe_fitexample.pdf')
plt.show()
