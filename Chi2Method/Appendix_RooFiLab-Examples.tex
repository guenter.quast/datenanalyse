\section{Beispiele mit \textit {RooFiLab}} \label{sec_examples}
Die folgenden Unterkapitel enthalten einfache Beispiele, die die Verwendung
von \textit{RooFiLab} illustrieren und als Basis f\"ur eigene Anwendungen 
dienen k\"onnen. 

\clearpage\newpage
\subsection{Geradenanpassung an Messdaten mit Fehlern in x und y}

Dieses Eingabedatei f\"ur \textit {RooFiLab} enth\"alt zahlreiche 
Kommentarzeilen und dokumentiert damit an einem einfachen Beispiel 
die verf\"ugbaren Optionen.  Durch die Zeile \verb+#! dofit = true+ wird
eine automatisierte Anpassung mit in den entsprechenden Zeilen mit der
Zeichenfolge \verb+#!+ gesetzten Optionen ausgef\"uhrt. 

Interessant an
diesem Beispiel ist es, die \"ubliche 
Geradengleichung \verb|m*x+b| durch
\verb|m*(x-5)+b| 
zu ersetzen - dies f\"uhrt zu deutlich reduzierten
Korrelationen der angepassten Parameter. 

\begin{verbatim}
# straight-line fit to data with errors in x and y, incl. simple correlations
# ===========================================================================
#! staterrors = xy
#! systerrors = 0.02 0.04 rel rel

#! fit = "m*x+b" "m,b" "roofilab.fit"
### fit = "m*(x-5.)+b" "m,b" "roofilab.fit" # führt zu kleineren Korrelationen
#! initialvalues = 0.015 0

### Befehl zum Ausf\"uhren des Fits
#! dofit = true

#! secondgraph = syst

#! title = "Anpassung an Daten mit korrelierten Fehlern"
#! graphlegend = "Daten" bottom right
#! functionlegend = "Funktion" bottom right
#! xaxis = "X-Werte"
#! yaxis = "Y-werte bzw. f(x)"

#! markersettings = 1.5 4 24
#! functionsettings = 1 3 2
#! grid = y
#! logscale = 0
#! savegraphic = "roofilab.eps"

# =================Eingabe der Daten ===================================
# values in up to four columns separated by whitespaces 
#       (except for linebreaks or linefeeds)
# x       y      ex      ey
4.05	0.035	0.12	0.006
4.36	0.056	0.13	0.007
4.68	0.052	0.09	0.005
4.80	0.044	0.09	0.005
5.09	0.048	0.14	0.007
5.46	0.055	0.14	0.007
5.71	0.066	0.17	0.009
5.83	0.048	0.21	0.011
6.44	0.075	0.22	0.011
8.09	0.070	0.28	0.014
8.72	0.097	0.32	0.016
9.36	0.080	0.37	0.018
9.60	0.120	0.39	0.020
\end{verbatim}

\clearpage \newpage 
\subsection{Einfache Mittelung von korrelierten Messungen}

Auch die Mittelung von Messdaten entspricht formal einer
$\chi^2$-Anpassung. Dazu wird als Funktion
einfach eine Konstante gew\"ahlt. Die im Beispiel 
gezeigten Messungen sind
vier individuelle Messungen der Masse des Z-Bosons 
am Beschleuniger LEP des CERN. Der allen Messungen
gemeinsame Fehler von 1.7\,MeV r\"uhrt von Unsicherheiten
der Schwerpunktesenergie des Beschleunigers her. Dieser
Fehler wird in der Zeile \\
\verb+ #! systerrors = 0 0.0017 abs abs + spezifiziert.

~\\

\begin{verbatim}
# Mesurements of Z-Mass by AELPH, DELPHI, L3 and OPAL
# ---------------------------------------------------

# graphics options 
#! markersettings = 1.5 4 24	
#! functionsettings = 1 3 3	
#! grid = y	
# logscale = 0	
# savegraphic = "roofilab.eps"	
# saverfl = "data.rfl"	

# plot lables
#! title = "averaging measurements"	
#! xaxis = "n"	
#! yaxis = "Mass of Z boson"	
#! graphlegend = "Z mass measurements" bottom right
#! functionlegend = "average Z mass" bottom right

# fit control
#! fit = "m" "m" "mittelung.fit"	
#! initialvalues =  91.2	
#! dofit = true	

#! staterrors = y	# control-command
#! systerrors = 0 0.0017 abs abs
# the data, LEP electroweak working group, CERN 2000    
1 91.1893 0.0031
2 91.1863 0.0028
3 91.1894 0.0030
4 91.1853 0.0029
\end{verbatim}

\clearpage \newpage 
\subsection{Polynom-Anpassung an Datenpunkte mit Poisson-Fehlern}
In diesem Beispiel wird die Anpassung eines Polynoms vierten Grades
an Datenpunkte mit unkorrelierten Fehlern gezeigt. Diese 
Aufgabe entspricht der Anpassung einer Winkelverteilung an
Daten eines Streuexperiments, bei dem (recht seltene) Ereignisse 
unter verschiedenen Winkeln gemessenen werden. Die Fehler sind
daher durch die statistischen Fehler dominiert, die f\"ur jeden
Messpunkt mit $n_i$ beobachteten Ereignissen durch 
$\sqrt{(n_i)}$ gegeben sind. Obwohl die Fehler nicht Gau\ss{}-f\"orimg
sind, liefert eine $\chi^2$-Anpassung oft akzeptable Ergebnisse.

Bei sehr kleiner Z\"ahlstatistik sollte allerdings als Fehlerverteilung
die Poisson-Verteilung zu Grunde gelegt werden. Dies ist mit
einer Log-Likelihoodanpassung in \textit{ROOT} m\"oglich. F\"ur die im Bin $i$ 
eines Histogramms beobachtete Zahl an Ereignissen wird
die Likelihood ${\cal{L}}_i=\rm(Poisson(n_i,\mu(x_i;\ve p))$ berechnet, 
$n_i$ Ereignisse zu beobachten, wenn $\mu(x_i,\ve p)$ erwartet wurden.
Die Gesamtlikelihood ergibt sich als Produkt \"uber alle Bins, dessen
negativer Logarithmus 
$-\ln( {\cal{L}})=-\log(\prod {\cal{L}}_i)$, 
bzgl. der Parameter $\ve p$ minimiert wird. In \textit{RooFiLab}
kann diese Option mit der Zeile 
\verb+#! fitmethod = likelihood+ 
gew\"ahlt werden; in diesem Fall werden die angegebenen statistischen
Fehler ignoriert bzw. k\"onnen auch weggelassen werden.
Aus technischen Gr\"unden (wegen der Verwendung der Histogramm-Klasse 
TH1) m\"ussen die x-Werte \"aquidistant gew\"ahlt werden. 

\begin{verbatim}
##########################################################
#     example: fit of an angular distribution
##########################################################

# plot commands
#! title = "angular distribution "
#! xaxis = "cos(theta)"
#! yaxis = "number of events"
#! graphlegend ="observed rate " top left 
#! functionlegend ="fitted cos(theta) distribution " top left 
#! markersettings = 1.5 2 5
#! functionsettings = 1 3 3    
# fit control
#! fit = "a4*x^4+a3*x^3+a2*x^2+a1*x+a0" "a0,a1,a2,a3,a4" "v_vs_cost.fit"
#! dofit = true

# fitmethod = likelihood # uncomment to perform a Log Likelihood fit

# definition of data
#! staterrors = y
# cost   N    sqrt(N)
-0.9    81.    9.0
-0.7    50.    7.1
-0.5    35.    5.9
-0.3    27.    5.2
-0.1    26.    5.1
 0.1    60.    7.7
 0.3   106.    10.3
 0.5   189.    13.7
 0.7   318.    17.8
 0.9   520.    22.8
\end{verbatim}

\clearpage \newpage 
\subsection{Mittelung von korrelierten Messungen mit Kovarianz-Matrix}
Als Beispiel f\"ur die Ber\"ucksichtigung von 
Korrelationen werden hier acht Messungen der Masse
des W-Bosons miteinander kombiniert. Die Messungen der 
vier LEP-Experimente in jeweils zwei Endzust\"anden 
haben unterschiedliche, teilweise zwischen allen
Messungen oder nur innerhalb der entsprechenden
Endzust\"ande korrelierte Unsicherheiten. Es ist deshalb
notwendig, die komplette 8$\times8$-Kovarianz-Matrix 
zu spezifizieren (s.u.), die in 
4$\times4$-Blockmatrizen zerf\"allt. Das geschieht in 
der Zeile \\
\verb+ #! covmatrices = 0 wmass.cov +.
 
\begin{verbatim}
# Mesurements of W-Mass by AELPH, DELPHI, L3 and OPAL
# ---------------------------------------------------
# ### example of fit with covariance matrix#
# --- graphics options 
#! markersettings = 1.5 4 24	
#! functionsettings = 1 3 3	
#! grid = y	
# logscale = 0	
# savegraphic = "graph.eps"	
# plot lables
#! title = "averaging measurements"	
#! xaxis = "n"	
#! yaxis = "Mass of W boson"	
#! graphlegend = "W mass measurements" top right	
#! functionlegend = "average W mass" top right	
# --- fit control
#! fit = "m" "m" "Wmittelung.fit"	
#! initialvalues =  80.5	
#! dofit = true	
# --- the data (LEP electroweak working group, CERN 2006)
#! staterrors = 0
#! systerrors = 0 0 abs abs
#! covmatrices = 0 wmass.cov
1 80.429  0.059 # qqlv ALEPH
2 80.340  0.076 # qqlv DELPHI
3 80.213  0.071 # qqlv L3
4 80.449  0.062 # qqlv OPAL
5 80.475  0.082 # qqqq ALEPH
6 80.310  0.102 # qqqq DELPHI
7 80.323  0.091 # qqqq L3
8 80.353  0.081 # qqqq OPAL

//file wmass.cov
  0.003481 0.000316 0.000316 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.005776 0.000316 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.000316 0.005041 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.000316 0.000316 0.003844 0.000383 0.000383 0.000383 0.000383
  0.000383 0.000383 0.000383 0.000383 0.006724 0.001741 0.001741 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.010404 0.001741 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.001741 0.008281 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.001741 0.001741 0.006561
\end{verbatim}


