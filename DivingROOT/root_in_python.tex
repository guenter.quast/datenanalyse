\chapter{ROOT in \python} \label{chap-pyroot} 
ROOT also offers an interface named 
\href{http://root.cern.ch/drupal/content/pyroot}
{PyRoot, see http://root.cern.ch/drupal/content/pyroot}, 
to the \python programming
language. \python is used in a wide variety of application areas and 
one of the most used scripting languages today. With its very high-level
data types with dynamic typing, its intuitive object 
orientation and the clear and efficient syntax \python is very  
suited to control even complicated analysis work flows. 
With the help of PyROOT it becomes possible to combine the power 
of a scripting language with ROOT methods. 

Introductory material to \python 
is available from many sources in the Internet, see  e.\,g. 
\href{http://docs.python.org/}{http://docs.python.org/}.  
There are additional very powerful \python packages, 
like \href{htp://www.numpy.org}{\textit{numpy}}, providing 
high-level mathematical functions and handling of large
multi-dimensional matrices, or 
\href{http://matplotlib.org}{\textit{matplotlib}}, providing
plotting tools for publication-quality graphics.
PyROOT additionally adds to this access to the vast capabilities 
of the ROOT universe. 

To use ROOT from \python{}, the environment variable
\verb#PYTHONPATH# must include the path to the library path, 
\verb#$ROOTSYS/lib#, of a ROOT version with \python support.
Then, \textit{PyROOT} provides direct 
interactions with ROOT classes 
from \python by importing ROOT.py into \python scrips via 
the command \verb#import ROOT#; it is also
possible to import only selected classes from ROOT, e.\,g. 
\verb#from ROOT import TF1#.

\section{PyROOT}

The access to ROOT classes and their methods in PyROOT is almost identical
to C++ macros, except for the special language features of \python{},
 most importantly dynamic type declaration at the time of assignment.  

Coming back to our first example, simply plotting a function in ROOT,
the following C++ code:
\begin{lstlisting}
TF1 *f1 = new TF1("f2","[0]*sin([1]*x)/x",0.,10.);
f1->SetParameter(0,1);
f1->SetParameter(1,1);
f1->Draw();
\end{lstlisting}
in \python becomes:
\begin{lstlisting}
import ROOT
f1 = ROOT.TF1("f2","[0]*sin([1]*x)/x",0.,10.)
f1.SetParameter(0,1)
f1.SetParameter(1,1)
f1.Draw();
\end{lstlisting}

A slightly more advanced example hands over data defined in the macro to 
the ROOT class \verb#TGraphErrors#. Note that a \python array can be
used to pass data between \python and ROOT. The first line in the \python 
script allows it
to be executed directly from the operating system, without the need
to start the script from \verb#python# or the highly recommended
powerful interactive shell \verb#ipython#. 
The last line in the python script is there to allow you to have 
a look at the graphical output in the ROOT canvas before it disappears 
upon termination of the script.  
 
\vfill

\pagebreak[3]
Here is the C++ version: 
\begin{lstlisting}
void TGraphFit() {
//
//Draw a graph with error bars and fit a function to it
//
gStyle->SetOptFit(111);  //superimpose fit results
// make nice Canvas 
TCanvas *c1 = new TCanvas("c1","Daten",200,10,700,500);
c1->SetGrid();
//define some data points ...
const Int_t n = 10;
Float_t x[n]  = {-0.22, 0.1, 0.25, 0.35, 0.5, 0.61, 0.7, 0.85, 0.89, 1.1};
Float_t y[n]  = {0.7, 2.9, 5.6, 7.4, 9., 9.6, 8.7, 6.3, 4.5, 1.1};
Float_t ey[n] = {.8,.7,.6,.5,.4,.4,.5,.6,.7,.8};
Float_t ex[n] = {.05,.1,.07,.07,.04,.05,.06,.07,.08,.05};
// and hand over to TGraphErros object
TGraphErrors *gr = new TGraphErrors(n,x,y,ex,ey);
gr->SetTitle("TGraphErrors with Fit");
gr->Draw("AP");
// now perform a fit (with errors in x and y!)
gr->Fit("gaus");
c1->Update();          
} 
\end{lstlisting}
\vspace{-1.5pc} \hfill\small file: \texttt{TGraphFit.C}\\~\\

In \python it looks like this:

\begin{lstlisting}
#!/usr/bin/env python
#
# Draw a graph with error bars and fit a function to it
#
from ROOT import gStyle, TCanvas, TGraphErrors 
from array import array
gStyle.SetOptFit(111)  # superimpose fit results
c1=TCanvas("c1","Data",200,10,700,500) #make nice Canvas 
c1.SetGrid()
#define some data points ...
x = array('f', (-0.22, 0.1, 0.25, 0.35, 0.5, 0.61, 0.7, 0.85, 0.89, 1.1) )
y = array('f', (0.7, 2.9, 5.6, 7.4, 9., 9.6, 8.7, 6.3, 4.5, 1.1) ) 
ey = array('f', (.8,.7,.6,.5,.4,.4,.5,.6,.7,.8) )
ex = array('f', (.05,.1,.07,.07,.04,.05,.06,.07,.08,.05) )
nPoints=len(x)
# ... and hand over to TGraphErros object
gr=TGraphErrors(nPoints,x,y,ex,ey)
gr.SetTitle("TGraphErrors with Fit")
gr.Draw("AP");
gr.Fit("gaus")
c1.Update()
# request user action before ending (and deleting graphics window)
raw_input('Press <ret> to end -> ')
\end{lstlisting}
\vspace{-1.5pc} \hfill\small file: \texttt{TGraphFit.py}\\~\\

Comparing the C++ and \python versions in these two examples, it 
now should be clear how easy it is to convert any ROOT Macro in
C++ to a \python version. 

As another example, let us revisit {\em macro3} from Chapter~\ref{chap-graphs}.
A straight-forward \python version relying on the ROOT class \verb|TMath|:
\lstinputlisting{macros/macro3.py}  
\vspace{-1.5pc} \hfill\small file: \texttt{macro3.py}\\~\\

\subsection {\bf More \python - less ROOT}
You may have noticed already that there are some \python modules
providing functionality similar to ROOT classes, which fit
more seamlessly into your \python code. 

A more ``pythonic'' version of the above {\texttt macro3} would 
use a replacement of the ROOT class \verb|TMath| 
for the provisoining of data to \verb|TGraphPolar|. 
With the {\em math} package, the part of the code becomes
\begin{lstlisting}
import math
from array import array
from ROOT import TCanvas,TGraphPolar
   ...
ipt=range(0,npoints)
r=array('d',map(lambda x: x*(rmax-rmin)/(npoints-1.)+rmin,ipt))
theta=array('d',map(math.sin,r))
e=array('d',npoints*[0.])
  ...
\end{lstlisting}
Using the very powerful package {\em numpy} and the built-in
functions to handle numerical arrays makes the \python code 
more compact and readable:
\begin{lstlisting}
import numpy as np
from ROOT import TCanvas,TGraphPolar
   ...
r=np.linspace(rmin,rmax,npoints)
theta=np.sin(r)
e=np.zeros(npoints)
   ...
\end{lstlisting}
\vspace{-1.5pc} \hfill\small file: \texttt{macro3\_numpy.py}\\~\\

{\bf Customised Binning}\\
This example combines comfortable handling of arrays in \python to define
variable bin sizes of a ROOT histogram. All we need to know is the
interface of the relevant ROOT class and its methods (from the ROOT
documentation):
\vskip -1pc \begin{lstlisting}
	TH1F(const char* name, const char* title, Int_t nbinsx, const Double_t* xbins)
\end{lstlisting}
	
Here is the \python code:
\vskip -1pc \begin{lstlisting}
import ROOT
from array import array
arrBins = array('d',(1,4,9,16) ) # array of bin edges
histo = ROOT.TH1F("hist", "hist", len(arrBins)-1, arrBins)
# fill it with equally spaced numbers
for i in range(1,16):
  histo.Fill(i)
histo.Draw()
\end{lstlisting}
\vspace{-1.5pc} \hfill\small file: \texttt{histrogram.py}\\~\\

%\subsection {Reading a Histogram from a File}
%
%\lstinputlisting{macros/read_a_histo.py}  
%\vspace{-.5pc} \hfill\small file: \texttt{read_a_histo.py} \\~\\
%

{\bf A fit example in \python using {\texttt TMinuit} from ROOT}

One may even wish to go one step further and do most of the 
implementation directly in \python{}, while using only some
ROOT classes. In the example below, the ROOT class
\verb#TMinuit# is used as the minimizer in a $\chi^2$-fit. 
Data are provided as \python arrays, the function to be fitted 
and the $\chi^2$-function are defined in \python 
and iteratively called by \texttt{Minuit}. The results are extracted
to \python objects, and plotting is done via the very powerful and
versatile python package \texttt{matplotlib}.
%
\lstinputlisting{macros/fitting-example.py}
\vspace{-1.5pc} \hfill\small file: \texttt{fitting-example.py}\\~\\

%\section{Matplotlib}



