#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro3 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot 
# last modified: 
#--------------------------------------------------------------
#
# *** Builds a polar graph in a square Canvas

from ROOT import TCanvas,TGraphPolar
from array import array

c=TCanvas("myCanvas","myCanvas",600,600)
rmin=0.
rmax=6.*TMath.Pi()
npoints=300
r=array('d',npoints*[0.])
theta=array('d',npoints*[0.])
e=array('d',npoints*[0.])
for ipt in range(0,npoints):
    r[ipt] = ipt*(rmax-rmin)/(npoints-1.)+rmin
    theta[ipt]=TMath.Sin(r[ipt])
grP1=TGraphPolar(npoints,r,theta,e,e)
grP1.SetTitle("A Fan")
grP1.SetLineWidth(3)
grP1.SetLineColor(2)
grP1.Draw("AOL")

input('Press <ret> to end -> ')

