#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro3 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot 
# last modified: 
#--------------------------------------------------------------
#
# *** Builds a polar graph in a square Canvas
#                    (with numpy package, no explicit loops needed)
import numpy as np
from ROOT import TCanvas,TGraphPolar

rmin=0.
rmax=6.*np.pi
npoints=300

r=np.linspace(rmin,rmax,npoints)
theta=np.sin(r)
e=np.zeros(npoints)

c=TCanvas("myCanvas","myCanvas",600,600)
grP1=TGraphPolar(npoints,r,theta,e,e)
grP1.SetTitle("A Fan")
grP1.SetLineWidth(3)
grP1.SetLineColor(2)
grP1.Draw("AOL")

input('Press <ret> to end -> ')

