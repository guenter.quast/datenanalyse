#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro2 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot, array  
# last modified: 
#--------------------------------------------------------------
#
# **** Reads the points from a file and produces a simple graph. */

from ROOT import gROOT,TCanvas,TGraphErrors,TLegend
from array import array

gROOT.SetStyle("Plain")
c=TCanvas()
c.SetGrid()
    
graph_expected=TGraphErrors("./macro2_input_expected.txt","%lg %lg %lg")
graph_expected.SetTitle("Measurement XYZ and Expectation;lenght [cm];Arb.Units")
graph_expected.SetFillColor(5) #5=kyellow
graph_expected.Draw("E3AL") # E3 draws the band
    
graph=TGraphErrors("./macro2_input.txt","%lg %lg %lg");
graph.SetMarkerStyle(24) # 20=kCircle
graph.SetFillColor(0)
graph.Draw("PESame") 
    
# Draw the Legend
leg=TLegend(.1,.7,.3,.9,"Lab. Lesson 2")
leg.SetFillColor(0)
leg.AddEntry(graph_expected,"Expected Points")
leg.AddEntry(graph,"Measured Points")
leg.Draw("Same")
    
c.Print("graph_with_band.pdf")

input('Press <ret> to end -> ')

