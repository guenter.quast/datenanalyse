#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro9 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot, numpy 
# last modified: 
#--------------------------------------------------------------
#
# *** Toy Monte Carlo example 
#    check pull distribution to compare 
#      - chi2 and 
#      - binned log-likelihood methods
#     in fits to histrogram data
from ROOT import gROOT,TH1F,TCanvas,gRandom,Double
import numpy as np

def pull(n_toys =10000,n_tot_entries=100, nbins=40,do_chi2=True):
  method_prefix="Log-Likelihood "
  if do_chi2:
    method_prefix="#chi^{2} "
#    // Create histo
  h4=TH1F(method_prefix+"h4",method_prefix+" Random Gauss",nbins,-4.,4.)
  h4.SetMarkerStyle(21)
  h4.SetMarkerSize(0.8)
  h4.SetMarkerColor(2)
#
#  book histograms for sigma and pull
  sigma=TH1F(method_prefix+"sigma",method_prefix+"sigma from gaus fit",50,0.5,1.5)
  pull = TH1F(method_prefix+"pull",method_prefix+"pull from gaus fit",50,-4.,4.)
#  Make nice canvases
  c0=TCanvas(method_prefix+"Gauss",method_prefix+"Gauss",0,0,320,240)
  c0.SetGrid()
  c1=TCanvas(method_prefix+"Result",method_prefix+"Sigma-Distribution",0,300,600,400);
  c0.cd()
#
  for i in range(0,n_toys): 
#  get random numbers ...
    a=np.random.normal(loc=0.,scale=1.,size=n_tot_entries)
    bincont, edges =np.histogram(a,bins=np.linspace(-4.,4.,num=nbins+1)) # histrogram in python
# hand over to root histogram (NOTE: SetContent expects underflows in first bin of array
    h4.SetContent(np.float64(np.insert(bincont,0,0))) # root needs Double type, starting from 2nd entry
    if(do_chi2):
      h4.Fit("gaus","q") # Chi2 fit
    else :
      h4.Fit("gaus","lq") # // Likelihood fit
#  some control output on the way 
    if(i%100==0):
      h4.Draw("ep")
      c0.Update()
#  Get mean, sigma from fit   
    fitfunc=h4.GetFunction("gaus")
    sig=fitfunc.GetParameter(2)
    mean=fitfunc.GetParameter(1)
    sigma.Fill(sig)
    pull.Fill(mean/sig * np.sqrt(n_tot_entries)) # end of toy MC loop
# ---- END loop over toys
#  show result
  c1.cd()
  pull.Fit("gaus")
  pull.Draw("EP")
  c1.Update()

# --- start of main program
gROOT.SetStyle("Plain");
n_toys=10000
n_tot_entries=100
n_bins=40
print "Performing Pull Experiment with chi2 \n";
pull(n_toys,n_tot_entries,n_bins,True)
print"Performing Pull Experiment with Log Likelihood\n";
pull(n_toys,n_tot_entries,n_bins,False)

raw_input('Press <ret> to end -> ')
    
