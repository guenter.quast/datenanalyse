#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro1 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot, array  
# last modified: 
#--------------------------------------------------------------
#
# **** Builds a graph with errors, displays it and saves it as image. *** */

from ROOT import TGraphErrors,TCanvas,gROOT,TF1,TLegend,TArrow,TLatex
from array import array

#The values and the errors on the Y axis
x_vals=array('f',(1,2,3,4,5,6,7,8,9,10) )
y_vals=array('f',(6,12,14,20,22,24,35,45,44,53) )
x_errs=array('f',(0.,0.,0.,0.,0.,0.,0.,0.,0.,0.) )
y_errs=array('f',(5,5,4.7,4.5,4.2,5.1,2.9,4.1,4.8,5.43) )
n_points=len(x_vals)

# Instance of the graph
graph=TGraphErrors(n_points,x_vals,y_vals,x_errs,y_errs)
graph.SetTitle("Measurement XYZ;lenght [cm];Arb.Units")

# Make the plot estetically better
gROOT.SetStyle("Plain")
graph.SetMarkerStyle(24) # 24=kOpenCircle
graph.SetMarkerColor(4) # 3=kBlue
graph.SetLineColor(4) # 

#The canvas on which we'll draw the graph    
mycanvas=TCanvas()

#Draw the graph !
graph.Draw("APE")

# Define a linear function
f=TF1("Linear law","[0]+x*[1]",.5,10.5)
# Let's make the funcion line nicer
f.SetLineColor(2) # 2=kRed
f.SetLineStyle(2)
# Fit it to the graph and draw it
graph.Fit(f)
f.Draw("Same")

# Build and Draw a legend
leg=TLegend(.1,.7,.3,.9,"Lab. Lesson 1")
leg.SetFillColor(0)
graph.SetFillColor(0)
leg.AddEntry(graph,"Exp. Points")
leg.AddEntry(f,"Th. Law")
leg.Draw("Same")

# Draw an arrow on the canvas 
arrow=TArrow(8,8,6.2,23,0.02,"----|>") 
arrow.SetLineWidth(2)
arrow.Draw()

# Add some text to the plot
text=TLatex(8.2,7.5,"#splitline{Maximum}{Deviation}")
text.Draw()

mycanvas.Print("graph_with_law.pdf")

input('Press <ret> to end -> ')
