#!/usr/bin/env python
#     (the first line allows execution directly from the linux shell) 
#
#-------- macro3 as python script ----------------------------
# Author:        G. Quast   Oct. 2013
# dependencies:  PYTHON v2.7, pyroot 
# last modified: 
#--------------------------------------------------------------
#
# *** Builds a polar graph in a square Canvas
#                     more "pythonic" version than macro3.py
import math
from array import array
from ROOT import TCanvas,TGraphPolar

rmin=0.
rmax=6.*math.pi
npoints=300

ipt=range(0,npoints)
r=array('d',map(lambda x: x*(rmax-rmin)/(npoints-1.)+rmin,ipt))
theta=array('d',map(math.sin,r))
e=array('d',npoints*[0.])

c=TCanvas("myCanvas","myCanvas",600,600)
grP1=TGraphPolar(npoints,r,theta,e,e)
grP1.SetTitle("A Fan")
grP1.SetLineWidth(3)
grP1.SetLineColor(2)
grP1.Draw("AOL")

input('Press <ret> to end -> ')

