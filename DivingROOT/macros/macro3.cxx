/* Builds a polar graph in a square Canvas
*/
void macro3(){
    double rmin=0;
    double rmax=TMath::Pi()*6;
    const int npoints=300;
    Double_t r[npoints];
    Double_t theta[npoints];
    for (Int_t ipt = 0; ipt < npoints; ipt++) {
        r[ipt] = ipt*(rmax-rmin)/(npoints-1.)+rmin;
        theta[ipt] = TMath::Sin(r[ipt]);
    }
    TCanvas* c = new TCanvas("myCanvas","myCanvas",600,600);
    TGraphPolar grP1 (npoints,r,theta);
    grP1.SetTitle("A Fan");
    grP1.SetLineWidth(3);
    grP1.SetLineColor(2);
    grP1.DrawClone("AOL");
}
