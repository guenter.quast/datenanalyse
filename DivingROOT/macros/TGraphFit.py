#!/usr/bin/env python

# -------------------------------------------------------
# Draw a graph with error bars and fit a function to it
#                            (adapted from TGraphFit.C)
#--------------------------------------------------------

from ROOT import gStyle, TCanvas, TGraphErrors 
from array import array

#set some global options
gStyle.SetOptFit(111)  # superimpose fit results

c1=TCanvas("c1","Daten",200,10,700,500) #make nice Canvas 
c1.SetGrid()

#define some data points ...
x = array('f', (-0.22, 0.1, 0.25, 0.35, 0.5, 0.61, 0.7, 0.85, 0.89, 1.1) )
y = array('f', (0.7, 2.9, 5.6, 7.4, 9., 9.6, 8.7, 6.3, 4.5, 1.1) ) 
ey = array('f', (.8,.7,.6,.5,.4,.4,.5,.6,.7,.8) )
ex = array('f', (.05,.1,.07,.07,.04,.05,.06,.07,.08,.05) )
nPoints=len(x)
# ... and pass to TGraphErros object
gr=TGraphErrors(nPoints,x,y,ex,ey)
gr.SetTitle("TGraphErrors mit Fit")
gr.Draw("AP");

# now perform a fit (with errors in x and y!)
gr.Fit("gaus")
c1.Update()

# request user action before ending (and deleting graphics window)
input('Press <ret> to end -> ')

