\chapter{Histograms}
\label{chap:histos}
\vspace{-5pc}
Histograms play a fundamental role in any type of Physics analysis, not only 
displaying measurements but being a powerful form of data reduction. 
ROOT presents many classes that represent histograms, all inheriting from the 
\verb|TH1| class. We will focus in this chapter on uni- and bi- dimensional 
histograms whose bin-contents are represented by floating point numbers 
\footnote{To optimise the memory usage you might go for one byte (TH1C), 
short (TH1S), integer (TH1I) or double-precision (TH1D) bin-content.}
, the \verb|TH1F| and \verb|TH2F| classes respectively.
%
\section{Your First Histogram}
Let's suppose that you want to measure the counts of a Geiger detector put 
in proximity of a radioactive source in a given time interval. 
This would give you an idea of the activity of your source.
The count distribution in this case is a Poisson distribution.
Let's see how operatively you can fill and draw a histogram in the 
following example macro.
%
\lstinputlisting{macros/macro5.cxx}
\vspace{-.5pc} \hfill\small file: \texttt{macro5.cxx}\\~\\
% 
Which gives you the following plot \ref{Fig:poisson_normalised}:
\begin{figure}
\begin{center}
\includegraphics[width=.5\textwidth]{figures/poisson}%
\end{center}
\caption[Poisson Distribution]{
\label{Fig:poisson_normalised}
  The result of a counting (pseudo) experiment.}
\end{figure}
Using histograms is rather simple. The main differences with respect to 
graphs that emerge from the example are:
\begin{itemize}
 \item line 5: The histograms have a name and a title right from the start, no 
predefined number of entries but a number of bins and a lower-upper range.
 \item line 15: An entry is stored in the histogram through the 
\textit{TH1F::Fill} method.
 \item line 19 and 22: The histogram can be drawn also normalised, ROOT 
automatically takes cares of the necessary rescaling.
 \item line 25 to 31: This small snippet shows how easy it is to access the 
moments and associated errors of a histogram.
\end{itemize}
%
\section{Add and Divide Histograms}
Quite a large number of operations can be carried out with histograms.
The most useful are addition and division.
In the following macro we will learn how to manage these procedures 
within ROOT.
%
\lstinputlisting{macros/macro6.cxx}  
\vspace{-.5pc} \hfill\small file: \texttt{macro6.cxx}\\~\\
% 
The plots that you will obtain are shown in \ref{Fig:histo_sum_ratio}
\begin{figure}
\begin{center}
\includegraphics[width=0.4\textwidth]{figures/histo_sum}%
\includegraphics[width=0.4\textwidth]{figures/histo_ratio}
\end{center}
\caption[Histograms Sum and Division]{\label{Fig:histo_sum_ratio}
  The sum of two histograms and the ratio.}
\end{figure}
Some lines now need a bit of clarification:
\begin{itemize}
 \item line 3: CINT, as we know,
is also able to interpret more than one function per file.
In this case the function simply sets up some parameters to conveniently 
set the line  of histograms.
 \item line 20 to 22: Some contracted C++ syntax for conditional statements is 
used to fill the histograms with different numbers of entries inside the loop.
 \item line 27: This is a crucial step for the sum and ratio of histograms
to handle errors properly. 
The method \textit{TH1::Sumw2 } causes the squares of weights to be stored
inside the histogram (equivalent to the number of entries per bin if 
weights of 1 are used). This information is needed to correctly calculate
the errors of each bin entry when the methods \textit{TH1::Add} 
and \textit{TH1::Divide} are applied. 
\item line 33: The sum of two histograms. A weight can be assigned to the added 
histogram, for example to comfortably switch to subtraction.
\item line 44: The division of two histograms is rather straightforward.
\item line 53 to 63: When you draw two quantities and their ratios, it is 
much better if all the information is condensed in one single plot. These 
lines provide a skeleton to perform this operation.
\end{itemize}
%
\section{Two-dimensional Histograms}
Two-dimensional histograms are a very useful tool, for example to inspect
correlations between variables. You can exploit the bi-dimensional histogram 
classes provided by ROOT in a very simple way. Let's see how in the following 
macro:
%
\lstinputlisting{macros/macro7.cxx}
\vspace{-.5pc} \hfill\small file: \texttt{macro macro7.cxx}\\~\\
% 
Two kinds of plots are provided by the code, the first one containing 
three-dimensional representations (Figure~\ref{Fig:2dhistos}) 
and the second one 
projections and profiles (\ref{Fig:proj_and_prof})
of the bi-dimensional histogram.
%
\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth, angle=90]{figures/th2f}%
\end{center}
\caption[Bi-dimensional histograms]{\label{Fig:2dhistos}
  Different ways of representing bi-dimensional histograms.}
\end{figure}
%
\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{figures/proj_and_prof}%
\end{center}
\caption[Projections and Profiles]{\label{Fig:proj_and_prof}
  The projections and profiles of bi-dimensional histograms.}
\end{figure}
%
When a projection is performed along the x (y) direction, for every bin along 
the x (y) axis, all bin contents along the y (x) axis are summed up (upper 
the plots of figure \ref{Fig:proj_and_prof}).
When a profile is performed along the x (y) direction, for every bin along 
the x (y) axis, the average of all the bin contents along the y (x) is 
calculated together with their RMS and displayed
as a symbol with error bar (lower two plots of figure).  
\ref{Fig:proj_and_prof}).

Correlations between the variables are quantified by the methods
\verb|Double_T GetCovariance()| \\
and \verb|Double_t GetCorrelationFactor()|. 

