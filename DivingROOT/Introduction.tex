\chapter{Motivation and Introduction}

\vspace{-5pc}
\begin{center}
   {\bf\large  \textit{Welcome to data analysis !}}
\end{center}

Comparison of measurements to theoretical models is one of the standard 
tasks in experimental physics. In the most simple case, a ``model'' 
is just a function providing predictions of measured data. Very often, 
the model depends on parameters. Such a model may simply state 
``the current $I$ is proportional to the voltage $U$'', and the 
task of the experimentalist consists of determining the resistance, 
$R$, from a set of measurements. 

As a first step, a visualisation of the data is needed. Next, some 
manipulations typically have to be applied, e.\,g. corrections  or 
parameter transformations. Quite often, these manipulations are 
complex ones, and a powerful library of mathematical functions and 
procedures should be provided - think for example of an integral or 
peak-search or a Fourier transformation applied to an input spectrum 
to obtain the actual measurement described by the model. 

One specialty of experimental physics are the inevitable errors affecting
each measurement, and visualization tools have to include these.   
In subsequent analysis, the statistical nature of the errors must 
be handled properly. 


As the last step, measurements are compared to models, and free model 
parameters need to be determined in this process , see 
Figure\ref{fig-examplefit} for an example of a function (model) fit to 
data points.
Several standard methods are available, and a data analysis tool should provide
easy access to more than one of them. Means to quantify the level of
agreement between measurements and model must also be available.

\begin{figure}[bht]
  \begin{center}
  \includegraphics[width=0.8\textwidth]{figures/examplefit}
  \caption{  \label{fig-examplefit}
   Measured data points with error bars and fitted quadratic function .}
  \end{center}
\end{figure}

\clearpage \newpage

Quite often, the data volume to be analyzed is large - think of 
fine-granular measurements accumulated with the aid of computers. 
A usable tool therefore must contain easy-to-use and efficient 
methods for data handling. 

In Quantum mechanics, models typically only predict the probability density 
function (``pdf'') of measurements depending on a number of parameters, and 
the aim of the experimental analysis is to extract the parameters from the 
observed distribution of frequencies at which certain values of the 
measurement are observed. Measurements of this kind require means to 
generate and visualize frequency distributions, so-called histograms, and  
stringent statistical treatment to extract the model parameters from
purely statistical distributions. 

Simulation of expected data is another important aspect in data analysis.
By repeated generation of ``pseudo-data'', which are analysed in the
same manner as intended for the real data,  analysis procedures can be
validated or compared. In many cases, the distribution of the measurement
errors is not precisely known, and simulation offers the possibility 
to test the effects of different assumptions.  


\section{Welcome to ROOT}

A powerful software framework addressing all of the above requirements 
is ROOT~\cite{ROOT}, an open source project
coordinated by the European Centre for Particle Physics, CERN in Geneva.
ROOT is very flexible and provides both a programming interface to use
in own applications and a graphical user interface for interactive data 
analysis. The purpose of this document is to serve as a beginners guide 
and provides extendable examples for your own use cases, based on
typical problems addressed in student labs.  This guide will hopefully
lay the ground for more complex applications in your future scientific
work building on a modern, state-of the art tool for data analysis.    

This guide in form of a tutorial is intended to introduce you to the 
ROOT package in about $50$ pages.
This goal will be accomplished using concrete 
examples, according to the ``learning by doing'' principle. Also because 
of this reason, this guide cannot cover the complexity of the ROOT package. 
Nevertheless, once you feel confident with the concepts presented in the 
following chapters, you will be able to appreciate the ROOT Users 
Guide~\cite{ROOT_Users_Guide} and 
navigate through the Class Reference~\cite{ROOT_Class_Reference} 
to find all the details you might be interested in. 
You can even look at the code itself, since ROOT is a free, open-source 
product. Use these documents in parallel to this tutorial!

The ROOT Data Analysis Framework itself is written in and heavily relys on
the programming language \verb|C++|, and therefore some 
knowledge about \verb|C| and\verb|C++| is required. 
Eventually, just profit from the immense 
available literature about \verb|C++| if you do not have any idea 
of what object oriented programming could be.

Recently, an alternative and very powerful way to use and control
ROOT classes via the interpreted high-level programming language 
\python became available. \python itself offers powerful modules
and packages for data handling, numerical applications and scienfific
computing. A vast number of bindings or wrappers to packages and 
tools written in other languages is also available. Access to the 
ROOT functionality is provided by the ROOT package 
\verb|PyRoot|~\cite{bib-pyroot}, allowing interactive work as
well as scritps based on \python. This is presented at the end 
of this guide in Chapter~\ref{chap-pyroot}.

ROOT is available for many platforms
(Linux, Mac OS X, Windows\dots), 
but in this guide we will implicitly assume that 
you are using Linux. The first thing you need to do with ROOT is 
install it. Or do you? Obtaining the latest ROOT version is 
straightforward. Just seek the ``Pro'' version on this webpage 
\url{http://root.cern.ch/drupal/content/downloading-root}. 
You will find precompiled versions for the different architectures, 
or the ROOT source code to compile yourself.  Just pick up the 
flavour you need and follow the installation instructions.
Or even simpler: use a virtual machine with ROOT installed ready for use,
as availalbe under e.\,g. 
\url{http://www-ekp.physik.uni-karlsruhe.de/~quast}. 

\begin{center}
  {\large\bf \textit{Let's dive into ROOT!}}
\end{center}
