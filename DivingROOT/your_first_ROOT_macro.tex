\chapter{ROOT Macros}
You know how other books go on and on about programming 
fundamentals and finally work up to building a complete, working program? 
Let's skip all that. In this part of the guide, we will describe macros 
executed by the ROOT C++ interpreter CINT. 

An alternative way to access
ROOT classes interactively or in a script will be shown in 
Chapter~\ref{chap-pyroot}, where we describe how to use 
the scritping language \python. This is most suitable for
smaller analysis projects, as some overhead of the C++ language
can be avoided. It is very easy to convert ROOT macros into
python scripts using the {\em pyroot} interface. 

Since ROOT itself is written 
in C++ let us start with Root macros in C++. 
As an additional advantage, it is relatively easy to turn a 
ROOT C++ macro into compiled -- and hence
much faster -- code, either as a pre-compiled library to load into ROOT, 
or as a stand-alone application, by adding some include statements
for header files or some ``dressing code'' to any macro. 


\section{General Remarks on ROOT macros}

If you have a number of lines which you were able to execute at the
ROOT prompt, they can be turned into a ROOT macro by giving them a 
name which corresponds to the file name without extension. The general 
structure for a macro stored in file \verb#MacroName.cxx# is
%
\begin{lstlisting}
void MacroName() {
        <          ...
          your lines of CINT code 
                   ...             >
}
\end{lstlisting}
%
The macro is executed by typing
\vskip -1pc \begin{lstlisting}
 > root MacroName.cxx 
\end{lstlisting}
at the system prompt, or it can be loaded into a ROOT session
and then be executed by typing
\vskip -1pc \begin{lstlisting}
root [0].L MacroName.cxx
root [1] MacroName();
\end{lstlisting}
at the ROOT prompt.
Note that more than one macro can be loaded this way, as 
each macro has a unique name in the ROOT name space.
Because many other macros may have been executed in the 
same shell before, it is 
a good idea to reset all ROOT parameters at the beginning
of a macro 
and define your preferred graphics options, e.\,g. with the
code fragment
\vskip -1pc \begin{lstlisting}
// re-initialise ROOT
gROOT->Reset();               // re-initialize ROOT
gROOT->SetStyle("Plain");     // set empty TStyle (nicer on paper)
gStyle->SetOptStat(111111);   // print statistics on plots, (0) for no output
gStyle->SetOptFit(1111);      // print fit results on plot, (0) for no ouput
gStyle->SetPalette(1);        // set nicer colors than default 
gStyle->SetOptTitle(0);       // suppress title box
   ...
\end{lstlisting}
Next, you should create a canvas for graphical output, 
with size, subdivisions and format suitable to your needs,
see documentation of class \verb#TCanvas#:
\begin{lstlisting}
// create a canvas, specify position and size in pixels
TCanvas c1("c1","<Title>",0,0,400,300);
c1.Divide(2,2); //set subdivisions, called pads
c1.cd(1); //change to pad 1 of canvas c1
\end{lstlisting}

These parts of a well-written macro are pretty standard,
and you should remember to include pieces
of code like in the examples above to make sure
your output always comes out as you had intended.

Below, in section\ref{sec-compilation}, some more 
code fragments will be shown, allowing you to use 
the system compiler to compile macros for more efficient
execution, or turn macros into stand-alone
applications linked against the ROOT libraries. 

\section{A more complete example}
Let us now look at a rather complete example of a typical task 
in data analysis, a macro that constructs a graph with errors, 
fits a (linear) model to it and saves it as an image. 
To run this macro, simply type in the shell:
\vskip -1pc \begin{lstlisting}
 > root macro1.cxx 
\end{lstlisting}
The code is build around the ROOT class
\verb|TGraphErrors|, which was already introduced previously. 
Have a look at it in the class reference guide, 
where you will also find further examples. The macro shown below 
uses additional classes, \verb|TF1| to define a function, 
\verb|TCanvas| to define size and properties of the window used 
for our plot, and \verb|TLegend| to add a nice legend. 
For the moment, ignore the commented include statements 
for header files, they will only become
important at the end (section~\ref{sec:compiling}).
%
\lstinputlisting{macros/macro1.cxx}  
\vspace{-.5pc} \hfill\small file: \texttt{macro1.cxx}\\~\\
%
Let's comment it in detail:
\begin{itemize}
 \item Line $11$: the name of the principal  function (it plays the role of 
the ``main'' function in compiled programs) in the macro file. 
It has to be the same as the file name without extension.
%
 \item Line $22-23$: instance of the \verb#TGraphErrors# class. 
 The constructor takes the number of points and the pointers to the 
 arrays of $x$~values, $y$~values, $x$~errors (in this case none, 
 represented by the NULL pointer) and $y$~errors.
 The second line defines in one shot the title of the graph and the 
 titles of the two axes, separated by a ``;''.
%
 \item Line $26-29$: the first line refers to the style of the plot, set as 
\textit{Plain}. This is done through a manipulation of the global variable 
\verb|gSystem| (ROOT global variables begin always with ``g''). 
The following three lines are rather intuitive right? 
To understand better the enumerators for 
colours and styles see the reference for the \verb|TColor| and 
\verb|TMarker| classes.
%  
  \item Line $32$: the canvas object that will host the drawn objects. The 
``memory leak'' is intentional, to make the object existing also out of the 
macro1 scope.
%
  \item Line $35$: the method \textit{DrawClone} draws a clone of the object 
on the canvas. It \textit{has to be} a clone, to survive after the scope 
of \verb|macro1|, and be displayed on screen after the end of the macro 
execution. The string option ``APE'' stands for:
  \begin{itemize}
    \item \textit{A} imposes the drawing of the Axes. 
    \item \textit{P} imposes the drawing of the graphs markers.
    \item \textit{E} imposes the drawing of the graphs markers errors.
  \end{itemize}
%
  \item Line $38$: define a mathematical function. There are several ways to 
accomplish this, but in this case the constructor accepts the name of 
the function, the formula, and the function range.
%
  \item Line $40$: maquillage. Try to give a look to the line 
styles at your disposal visiting the documentation of the \verb|TLine| 
class.
%
  \item Line $42$: fits the \textit{f} function to the graph, observe 
that the pointer is passed. It is more interesting to look at the output 
on the screen to see the parameters values and other crucial information 
that we will learn to read at the end of this guide.
%
  \item Line $43$: again draws the clone of the object on the canvas. The 
``Same'' option avoids the cancellation of the already drawn objects, in our 
case, the graph.
%
  \item Line $46-51$: completes the plot with a legend, represented by a 
\verb|TLegend| instance. The constructor takes as parameters the lower left and 
upper right corners coordinates with respect to the total size of the canvas, 
assumed to be 1, and the legend header string.
You can add to the legend the objects, previously drawn or not drawn, through 
the \verb|addEntry| method. Observe how the legend is drawn at the end: looks 
familiar now, right?
%
  \item Line $54-56$: defines an arrow with a triangle on the right hand side, 
  a thickness of 2 and draws it.
%
  \item Line $59-61$: interpret a Latex string which hast its lower left corner 
  located in the specified coordinate. The ``\#splitline\{\}\{\}'' construct 
  allows to store multiple lines in the same \verb|TLatex| object.
%
  \item Line $62$: save the canvas as image. The format is automatically 
inferred from the file extension (it could have been eps, gif, \dots).
\end{itemize}
%
Let's give a look to the obtained plot in figure~\ref{macro1_plot}. 
Beautiful outcome for such a small bunch of lines, isn't it?
\begin{figure}[h]
  \begin{center}
  \includegraphics[width=0.8\textwidth]{figures/graph_with_law}
  \caption{  \label{macro1_plot}
    Your first plot with data points.}
  \end{center}
\end{figure}

A version of the same macro in \python is available in the
file \texttt{macro1.py}; you may want to open it in the editor
and have a look at the differences right now - please consult 
the introductory sections of Chapter~\ref{chap-pyroot} first. 
This example shows how easy it is to change a ROOT macro 
from C++ to \python. 

\section{Summary of Visual effects}
\subsection{Colours and Graph Markers}
We have seen that to specify a colour, some identifiers like kWhite, kRed or 
kBlue can be specified for markers, lines, arrows etc. 
The complete summary of colours is represented by the 
ROOT ``colour wheel'', shown in appendix in figure~\ref{fig:colour_wheel}. 
To know more about the full story, refer to the online documentation 
of \verb|TColor|.
\\
ROOT provides an analogue of the colour wheel for the graphics markers. 
Select the most suited symbols for your
plot (see Figure~\ref{fig:colour_wheel}) among dots, triangles, 
crosses or stars.
An alternative set of names for the markers is summarised in 
Table~\ref{tab:markers}.
%
\subsection{Arrows and Lines}
The macro line 56 shows how to define an arrow and draw it. 
The class representing arrows is \verb|TArrow|, which inherits 
from \verb|TLine|. 
The constructors of lines and arrows always contain the coordinates of 
the endpoints. Arrows also foresee parameters to specify their 
shapes (see Figure~\ref{fig:arrows}).
Do not underestimate the role of lines and arrows in your plots. 
Since each plot should contain a message, it is convenient to stress it with 
additional graphics primitives. 

\subsection{Text}
Also text plays a fundamental role in making the plots self-explanatory. 
A possibility to add text in your plot is provided by the \verb|TLatex| class.
The objects of this class are constructed with the coordinates of the 
bottom-left corner of the text and a string which contains the text itself. 
The real twist is that ordinary Latex mathematical symbols are automatically 
interpreted, you just need to replace the ``\textbackslash'' 
by a ``\#'' (see Figure ~\ref{fig:tlatex}).


\section{Interpretation and Compilation}\label{sec-compilation}
\label{sec:compiling}
%
As you observed, up to now we heavily exploited the capabilities of ROOT for 
interpreting our code, more than compiling and then executing. This is 
sufficient for a wide range of applications, but you might have already asked 
yourself ``how can this code be compiled?''. There are two answers.
%
\subsection{Compile a Macro with ACLiC}
ACLiC will create for you a compiled dynamic library for your macro, without 
any effort from your side, except the insertion of the appropriate header
files in lines 3--9. In this example, they are already included. This does
not harm, as they are not loaded by CINT. To generate an object libary
from the macro code, from inside the interpreter type (please note the ``+''):
\vskip -1pc \begin{lstlisting}
 root [1] .L macro1.cxx+
\end{lstlisting}
Once this operation is accomplished, the macro symbols will be available in 
memory and you will be able to execute it simply by calling from inside the 
interpreter:
\vskip -1pc \begin{lstlisting}
 root [2] macro1()
\end{lstlisting}
%
\subsection{Compile a Macro with g++}
In this case, you have to include the appropriate headers in the code and 
then exploit the \textit{root-config} tool for the automatic settings of all 
the compiler flags. \textit{root-config} is a script that comes with ROOT; 
it prints all flags and libraries needed to compile code and link it with 
the ROOT libraries. In order to make the code executable stand-alone,
an entry point for the operating system is needed, in C++ this is the
procedure \verb#int main();#. The easiest way to turn
a ROOT macro code into a stand-alone application is to add the following 
``dressing code'' at the end of the macro file. This defines the
procedure main, the only purpose of which is to call your macro:

\vskip -1pc \begin{lstlisting}
#ifndef __CINT__
int main() {
  ExampleMacro();
  return 0;
}
#endif
\end{lstlisting}
%
Within ROOT, the symbol \verb#__CINT__# is defined, and the 
code enclosed by \verb+#ifndef __CINT__+ and \verb+#endif+ is not executed;
on the contrary, when running the system compiler \verb#g++#, 
this symbol is not defined, and the code is compiled. To create a
stand-alone program from a macro called \verb#ExampleMacro.C#, 
simply type
\vskip -1pc \begin{lstlisting}
 > g++ -o ExampleMacro.exe ExampleMacro.C `root-config --cflags --libs`
\end{lstlisting}
and execute it by typing
\vskip -1pc \begin{lstlisting}
> ./ExampleMacro.exe
\end{lstlisting}

This procedure will, however, not give access to the ROOT graphics, 
as neither control of mouse or keyboard events nor access to the
graphics windows of ROOT is available. If you want your stand-alone
application have display graphics output and respond to mouse and
keyboard, a slightly more complex piece of code can be used. In the
example below, a macro \verb#ExampleMacro_GUI# is executed by the ROOT 
class TApplication. As a further feature, this code example offers access 
to parameters eventually passed to the program when started from the
command line.  Here is the code fragment:

%
\vskip -1pc \begin{lstlisting}
#ifndef __CINT__
void StandaloneApplication(int argc, char** argv) {
  // eventually, evaluate the application parameters argc, argv
  // ==>> here the ROOT macro is called
  ExampleMacro_GUI();
}
  // This is the standard "main" of C++ starting a ROOT application
int main(int argc, char** argv) {
   gROOT->Reset();
   TApplication app("Root Application", &argc, argv);
   StandaloneApplication(app.Argc(), app.Argv());
   app.Run();
   return 0;
}
#endif
\end{lstlisting}
%
Compile the code with
\vskip -1pc \begin{lstlisting}
 > g++ -o ExampleMacro_GUI.exe ExampleMacro_GUI `root-config --cflags --libs`
\end{lstlisting}
and execute the program with
\vskip -1pc \begin{lstlisting}
> ./ExampleMacro_GUI.exe
\end{lstlisting}
