\chapter{ROOT Basics}
Now that you have installed ROOT, what's this interactive shell 
thing you're running?
It's like this: ROOT leads a double life. It has an interpreter for macros 
(CINT~\cite{cint}) that you can run from the command line or run like 
applications. 
But it is also an interactive shell that can evaluate arbitrary statements and 
expressions. 
This is extremely useful for debugging, quick hacking and testing. 
Let us first have a look at some very simple examples.

\section {ROOT as calculator}
You can even use the ROOT interactive shell in lieu of a calculator!
Launch the ROOT interactive shell with the command
\vskip -1pc \begin{lstlisting}
 > root
\end{lstlisting}
on your Linux box. The prompt should appear shortly:
\vskip -1pc \begin{lstlisting}
 root [1] 
\end{lstlisting}
and let's dive in with the steps shown here:
\vskip -1pc \begin{lstlisting}
root [0] 1+1
(const int)2
root [1] 2*(4+2)/12.
(const double)1.00000000000000000e+00
root [2] sqrt(3)
(const double)1.73205080756887719e+00
root [3] 1 > 2
(const int)0
root [4] TMath::Pi()
(Double_t)3.14159265358979312e+00
root [5] TMath::Erf(.2)
(Double_t)2.22702589210478447e-01
\end{lstlisting}
Not bad. You can see that ROOT offers you the possibility not only to type in 
\verb|C++| statements, but also advanced mathematical functions, which live 
in the \verb+TMath+ namespace.

Now let's do something more elaborated. A numerical example with the well 
known geometrical series:
\vskip -1pc \begin{lstlisting}
root [6] double x=.5
root [7] int N=30
root [8] double geom_series=0
root [9] for (int i=0;i<N;++i)geom_series+=TMath::Power(x,i)
root [10] TMath::Abs(geom_series - (1-TMath::Power(x,N-1))/(1-x))
(Double_t)1.86264514923095703e-09
\end{lstlisting}
Here we made a step forward. We even declared variables and used a \textit{for} 
control structure. 
Note that there are some subtle differences between CINT and the 
standard C++ language. You do not need the ";'' at the end of line 
in interactive mode -- try the difference 
e.g. using the command at line \verb#root [6]#.
  
\section {ROOT as Function Plotter}
Using one of ROOT's powerful classes, here \verb#TF1#
\footnote{All ROOT classes start with the letter  T.}, 
will allow us to display a function of one variable, $x$. Try the following:
\vskip -1pc \begin{lstlisting}
root [11] TF1 *f1 = new TF1("f1","sin(x)/x",0.,10.);
root [12] f1->Draw();
\end{lstlisting}

\verb+f1+ is a pointer to an instance of a TF1 class, the arguments are used
 in the constructor; the first one of type string is a name to be entered 
in the internal ROOT memory management system, the second string type 
parameter defines the function, here \verb#sin(x)/x#, and the two 
parameters of type real define
the range of the variable $x$. The \verb#Draw()# method, here without any
parameters, displays the function in a window which should pop up after you
typed the above two lines. Note again differences between CINT and C++:
you could have omitted the ";'' at the end of lines, of CINT woud have
accepted the "." to access the method \verb#Draw()#. However, it is
best to stick to standard C++ syntax and avoid CINT-specific code,
as will become clear in a moment. 

A slightly extended version of this example is the definition of a function
with parameters, called [0], [1] and so on in ROOT formula syntax. We now need
a way to assign values to these parameters; this is achieved with the
method
\verb|SetParameter(<parameter_number>,<parameter_value>)| of
class \verb|TF1|. Here is an example:
\vskip -1pc \begin{lstlisting}
root [13] TF1 *f1 = new TF1("f2","[0]*sin([1]*x)/x",0.,10.);
root [14] f1->SetParameter(0,1);
root [15] f1->SetParameter(1,1);
root [16] f1->Draw();
\end{lstlisting}
Of course, this version shows the same results as the initial one. Try
playing with the parameters and plot the function again. 
The class \verb#TF1# has a large number of very useful methods, including
integration and differentiation. To make full use of this and other ROOT
 classes, visit the documentation on the Internet under  
\url{http://root.cern.ch/drupal/content/reference-guide}. 
Formulae in ROOT are evaluated using the class \verb#TFormula#,
so also look up the relevant class documentation for examples, 
implemented functions and syntax. 

On many systems, this class reference-guide is
available locally, and you should definitely download it to your own system to
have it at you disposal whenever you need it.

To extend a little bit on the above example, consider a more complex 
function you would like to define. You can also do this using standard \verb#C#
or \verb#C++# code. In many cases this is the only practical way, as the 
ROOT formula interpreter has clear limitations concerning complexity and 
speed of evaluation.

Consider the example below, which calculates and displays
the interference pattern produced by light falling on a multiple slit.
Please do not type in the example below at the ROOT command line, there 
is a  much simpler way:
Make sure you have the file \verb#slits.cxx# on disk, and
type \verb#root slits.cxx# in the shell. This will start
root and make it read the ``macro'' \verb#slit.cxx#, i.\,e. all the 
lines in the file will be executed one after the other. 
%
\lstinputlisting{macros/slits.cxx}  
\vspace{-.5pc} \hfill\small file: \texttt{slits.cxx}\\~\\
%

\begin{wrapfigure}{l}{0.6\linewidth} 
%\begin{figure}[htb]
  \begin{center}
  \includegraphics[width=0.9\linewidth]{figures/TF1_DoubleSlit}
  \caption{\label{fig-TF1_DoubleSlit}
    Output of macro slits.cxx with parameters 0.2 and 2.}
  \end{center}
\end{wrapfigure}
%\end{figure}

The example first asks for user input,
namely the ratio of slit width over slit distance, and the number
of slits. After entering this information, you should see 
the graphical output as is shown in Figure~\ref{fig-TF1_DoubleSlit} below. 

This is a more complicated example than the ones we have seen before, so 
spend some time analysing it carefully, you should have understood it before
continuing. Let us go through in detail:\\

Lines 6-19 define the necessary functions in \verb#C++# code, split into 
three separate functions, as suggested by the problem considered. 
The full interference pattern is given by the product of a function 
depending on the ratio of the width and distance of the slits, and a 
second one depending on the number of slits. More important for us
here is the definition of the interface of these functions to make them
usable for the ROOT class \verb#TF1#: the first argument is the pointer 
to $x$, the second one points to the array of parameters. 

The main program starts in line 17 with the definition of a function 
\verb#slits()# of type \verb#void#. 
After asking for user input, a ROOT function is defined 
using the C-type function given in the beginning.  We can now use all 
methods of the \verb#TF1# class to control the behaviour of our 
function -- nice, isn't it?  

If you like, you can easily extend the example to also plot the interference
pattern of a single slit, using function \verb#double single#, or of
a grid with narrow slits, function \verb#double nslit0#, in \verb#TF1#
instances.

Here, we used a macro, some sort of lightweight program, that the
interpreter distributed with ROOT, CINT, is able to execute. This is a 
rather extraordinary situation, since C++ is not natively an interpreted 
language! There is much more to say, therefore there is a dedicated 
chapter on macros.

\section {Controlling ROOT}
One more remark at this point:
as every command you type into ROOT is usually interpreted by CINT, an 
``escape character'' is needed to pass commands to ROOT directly. This 
character is the dot at the beginning of a line:
%
\vskip -1pc \begin{lstlisting}
root [1] .<command>
\end{lstlisting}
%
To 
\begin{itemize}
\item {\bf quit root}, simply type \verb#.q#
\item obtain a {\bf list of commands}, use \verb#.?#
\item {\bf access the shell} of the operating system, type
  \verb#.!<OS_command>#; try, e.\,g. \verb#.!ls# or \verb#.!pwd# 
\item {\bf execute a macro}, enter \verb#.x <file_name>#; in the above
  example, you might have used \verb#.x slits.cxx# at the ROOT prompt
\item {\bf load a macro}, type \verb#.L <file_name>#; in the above
  example, you might instead have used the command \verb#.L slits.cxx#
  followed by the function call \verb#slits();#. Note that after loading a
  macro all functions and procedures defined therein are available at
  the ROOT prompt.
\end{itemize}

\section {Plotting Measurements}
To display measurements in ROOT, including errors, there exists
a powerful class \verb#TGrapErrors# with different types of
constructors. In the example here, we use data from the file
\verb#ExampleData.txt# in text format:
%
\vskip -1pc \begin{lstlisting}
root [0] TGraphErrors *gr=new TGraphErrors("ExampleData.txt");
root [1] gr->Draw("AP");
\end{lstlisting}
You should see the output shown in Figure~\ref{fig-TGraphErrors_Example}. 

\begin{wrapfigure}{l}{0.6\linewidth} 
%\begin{figure}[htb]
   \begin{center}
  \includegraphics[width=0.9\linewidth]{figures/TGraphErrors_Example}
  \caption{ \label{fig-TGraphErrors_Example}
    Visualisation of data points with errors using the class
   TGraphErrors}
  \end{center}
\end{wrapfigure}
%\end{figure}

Make sure the file  \verb#ExampleData.txt#
is available in the directory from which you started ROOT. 
Inspect this file now with your favourate editor, or use the command
\verb#less ExampleData.txt# to inspect the file, you will see that the
format is very simple and easy to understand.
Lines beginning with \verb+#+ are ignored, very convenient to
add some comments on the type of data.  The data itself consist
of lines with four real numbers each, representing the
x- and y- coordinates and their errors of each data point.
You should quit 

The argument of the method \verb#Draw("AP")# is important here. It tells
the \verb#TGraphPainter# class to show the axes and to plot markers 
at the $x$ and $y$ positions of the specified data points. Note that this 
simple example relies on 
the default settings of ROOT, concerning the size of the canvas
holding the plot, the marker type and the line colours and thickness
used and so on. In a well-written, complete example, all this 
would need to be specified explicitly in order to obtain nice and
reproducible results. A full chapter on graphs will explain many more
of the features of the class \verb#TGraphErrors# and its relation
to other ROOT classes in much more detail. 

\section{Histograms in ROOT}
Frequency distributions in ROOT are handled by a set of classes derived
from the histogram class \verb#TH1#, in our case \verb#TH1F#. 
The letter \verb#F# stands for "float", meaning that the data 
type \verb#float# is used to store the entries in one histogram bin.
\begin{lstlisting}
root [0] TF1 efunc("efunc","exp([0]+[1]*x)",0.,5.);
root [1] efunc.SetParameter(0,1);
root [2] efunc.SetParameter(1,-1);
root [3] TH1F* h=new TH1F("h","example histogram",100,0.,5.);
root [4] for (int i=0;i<1000;i++) {h->Fill(efunc.GetRandom());}
root [5] h->Draw();
\end{lstlisting}

The first three lines of this example define a function, an exponential 
in this case, and set its parameters. In Line 4 a histogram is instantiated,
with a name, a title, a certain number of 100 bins (i.\,e. equidistant, 
equally sized intervals) in the range from 0. to 5. 

\begin{wrapfigure}{l}{0.6\linewidth} 
%\begin{figure}[htb]
  \begin{center}
  \includegraphics[width=0.9\linewidth]{figures/TH1F_Example}
  \caption{  \label{fig-TH1F_Example}
     Visualisation of a histogram filled with exponentially 
     distributed, random  numbers.}
  \end{center}
\end{wrapfigure}
%\end{figure}

We use yet another 
new feature of ROOT to fill this histogram with data, namely pseudo-random 
numbers generated with the method \verb#TF1::GetRandom#, which in turn
uses an instance of the  ROOT class \verb#TRandom# created when ROOT is 
started. Data is entered in the histogram 
in line 5 using the method \verb#TH1F::Fill# in a loop construct. 
As a result, the histogram is filled
with 1000 random numbers distributed according to the defined function. 
The histogram is displayed using the method \verb#TH1F::Draw()#.
You may think of this example as repeated measurements of the
life time of a quantum mechanical state, which are entered into the
histogram, thus giving a visual impression of the probability density
distribution. The plot is shown in Figure~\ref{fig-TH1F_Example}.

Note that you will not obtain an identical plot when executing the
above lines, depending on how the random number generator is initialised.

The class \verb#TH1F# does not contain a convenient input format from 
plain text files. The following lines of C++ code do the job.
One number per line stored in the text file ``expo.dat'' is read in
via an input stream and filled in the histogram until end of file
is reached.  
\begin{lstlisting}
root [1] TH1F* h=new TH1F("h","example histogram",100,0.,5.);
root [2] ifstream inp; double x;
root [3] inp.open("expo.dat");
root [4] while(!(inp >> x)==0){h->Fill(x);}  
root [5] h->Draw();
root [6] inp.close();
\end{lstlisting}

Histograms and random numbers are very important tools in statistical
data analysis, and the whole Chapter~\ref{chap:histos} will be dedicated to 
this. 

\section{Interactive ROOT}
Look at one of your plots again and
move the mouse across. You will notice that this is much more than a static
picture, as the mouse pointer changes its shape when touching objects 
on the plot. When the mouse is over an object, 
a right-click opens a pull-down menu displaying in the top line
the name of the ROOT class you are dealing with, e.g. 
\verb#TCanvas# for the display window itself,
\verb#TFrame# for the frame of the plot,  
\verb#TAxis# for the axes, 
\verb#TPaveText# for the plot name. 
Depending on which plot you are investigating, menus for the ROOT 
classes \verb#TF1#, \verb#TGraphErrors# or \verb#TH1F# will show up
when a right-click is performed on the respective graphical representations.
The menu items allow direct access to the members of the various
classes, and you can even modify them, e.g. change colour and
size of the axis ticks or labels, the function lines, marker types
and so on. Try it!

\begin{wrapfigure}{l}{0.6\linewidth} 
\includegraphics[width=0.99\linewidth]{figures/ROOTPanel_SetParameters}
  \caption{  \label{fig-ROOTPanel_SetParameters}
    Interactive ROOT panel for setting function parameters.}
\end{wrapfigure}

You will probably like the following: in the output produced 
by the example \verb#slits.cxx#, right-click on the
function line and select "SetLineAttributes", then 
left-click on "Set Parameters". This gives access  to a panel
allowing  you to interactively change  the parameters of the
function, as shown in Figure~\ref{fig-ROOTPanel_SetParameters}.
Change the slit width, or go from one to two and
then three or more slits, just as you like.  When clicking on
"Apply", the function plot is updated to reflect the actual
value of the parameters you have set. 

\clearpage

\begin{wrapfigure}{r}{0.4\textwidth} 
  \includegraphics[width=0.99\linewidth]{figures/ROOTPanel_FitPanel} \\
  \vskip -1pc \caption{  \label{fig-ROOT_FitPanel}
    Fit functions to graphs and histograms.\vspace{-3pc}}
\end{wrapfigure}

Another very useful interactive tool is the \verb#FitPanel#, available
for the classes \verb#TGraphErrors# and \verb#TH1F#. Predefined 
fit functions can be selected from a pull-down menu, 
including 
``\verb#gaus#'', ``\verb#expo#'' and ``\verb#pol0#'' - ``\verb#pol9#'' 
for Gaussian and exponential functions or polynomials of degree 0 to 9,
respectively. In addition, user-defined functions using the same syntax 
as for functions with parameters are possible.

After setting the initial 
parameters, a fit of the selected func\-tion to the data of a graph or 
histogram can be performed and the result displayed on the plot.
The fit panel is shown in Figure~\ref{fig-ROOT_FitPanel}. The fit
panel has a large number of control options to select the fit method,
fix or release individual paramters in the fit, to steer the level of 
output printed on the console, or to extract and display
additional information like contour lines showing parameter 
correlations. Most of the methods of the class 
\verb#TVirtualFitter# are easily available through the latest
version of the graphical interface. As function fitting is
of prime importance in any kind of data analysis, this
topic will again show up in later chapters. 
 
If you are satisfied with your plot, you probably want to save it.
Just close all selector boxes you opened previously, and select
the menu item \fbox{Save as} from the menu line of the window, which will
pop up a file selector box to allow you to choose the format, file 
name and target directory to store the image. 

There is one 
very noticeable feature here: you can store a plot as a root
macro! In this macro, you find the C++ representation of all 
methods and classes involved in generating the plot. This is a
very valuable source of information for your own macros, which 
you will hopefully write after having worked through this
tutorial. 

Using the interactive capabilities of ROOT is very useful for
a first exploration of possibilities. Other ROOT classes you
will be encountering in this tutorial have such graphical
interfaces as well. We will not comment further on this, 
just be aware of the existence of interactive features in ROOT
and use them if you find convenient. 
Some trial-and-error is certainly necessary
to find your way through the enormous number of menus
and possible parameter settings. 

\section{ROOT Beginners' FAQ}
At this point of the guide, some basic question could have already come 
to your mind. We will try to clarify some of them with further 
explanations in the following.

\subsection{ROOT type declarations for basic data types}

In the official ROOT documentation, you find special data
types replacing the normal ones, e.\,g.
\verb#Double_t#, \verb#Float_t# or  \verb#Int_t# replacing
the standard  \verb#double#, \verb#float# or \verb#int#
types. Using the ROOT types makes it easier to port code
between platforms (64/32 bit) or operating systems 
(windows/Linux), as these types are mapped to
suitable ones in the ROOT header files. If you want
adaptive code of this type, use the ROOT type declarations.
However, usually you do not need such adaptive code, and
you can safely use the standard C type declarations for your
private code, as
we did and will do throughout this guide. If you intend to become a
ROOT developer, however, you better stick to the official
coding rules!

\subsection{Configure ROOT at start-up}
\label{sec:config}
If the file \verb#.rootlogon.C# exists in your home directory,
it is executed by ROOT at start-up. Such a file can be
used to set preferred options for each new ROOT session.
The ROOT default for displaying graphics looks OK on the 
computer screen, but rather ugly on paper.
If you want to use ROOT graphs in documents, you should
change some of the default options. This is done most
easily by creating a new \verb#TStyle# object with
your preferred settings, as described in the class
reference guide, and then use the command 
\verb#gROOT->SetStyle("MyStyle");# to make this new
style definition the default one. As an example, have
a look in the file \verb#rootlogon.C# coming with this
tutorial. 

There is also a possibility to set many ROOT features, in 
particular those closely related to the operating and window
system, like  e.g. the fonts to be used, where to find start-up files,
or where to store a file containing the command history, and many 
others. The file searched for at ROOT start-up 
is called \verb#.rootrc# and must reside in 
the user's home directory; reading and interpeting this file 
is handled by the ROOT class \verb#TEnv#, see its documentation
if you need such rather advanced features.  

\subsection{ROOT command history}
Every command typed at the ROOT prompt is stored in a
file \verb#.root_hist# in your home directory. ROOT 
uses this file to allow for navigation in the 
command history with the up-arrow and down-arrow keys.
It is also convenient to extract successful
ROOT commands with the help of a text editor for use
in your own macros.

\subsection{ROOT Global Variables}

All global variables in ROOT begin with a small ``g''. Some of them 
were already implicitly introduced (for example in session~\ref{sec:config}). 
The most important among them are presented in the following:
\begin{itemize}
 \item \textbf{\href{http://root.cern.ch/root/html/TROOT.html}{gROOT}}: 
the gROOT variable is the entry point to the ROOT system.
Technically it is an instance of the \verb|TROOT| class.
Using the gROOT pointer one has access to basically every object created 
in a ROOT based program. 
The \verb|TROOT| object is essentially a container of several lists pointing 
to the main \verb|ROOT| objects.

 \item \textbf{\href{http://root.cern.ch/root/html/TRandom3.html}{gRandom}}: 
the gRandom variable is a variable that points to a random number generator 
instance of the type \verb|TRandom3|. 
Such a variable is useful to access in every point of a program
the same random number generator, in order to achieve a good quality 
of the random sequence.

 \item \textbf{\href{http://root.cern.ch/root/html/TStyle.html}{gStyle}}:
By default ROOT creates a default style that can be accessed via the 
\verb#gStyle# pointer.
This class includes functions to set some of the following object attributes.
\begin{itemize}
    \item Canvas
    \item Pad
    \item Histogram axis
    \item Lines
    \item Fill areas
    \item Text
    \item Markers
    \item Functions
    \item Histogram Statistics and Titles
\end{itemize}
 \item \textbf{\href{http://root.cern.ch/root/html/TSystem.html}{gSystem}}: 
An instance of a base class defining a generic interface to 
the underlying Operating System, in our case \verb|TUnixSystem|.
\end{itemize}



\vspace{5pc}
At this point you have already learnt quite a bit about 
some basic features of ROOT.
\begin{center}
{\bf\large \textit{Please move on to become an expert!}}
\end{center} 

