\chapter{RooFiLab}\label{chap:RooFiLab}
\section{Root-based tool for fitting: RooFiLab}\label{sec:RooFiLab}

Although simple in principle, the fomulation of a problem in 
\verb|C++| and the complex environment of the ROOT framework pose a
relativly high hurdle to overcome for the beginner. A simplification
and extension of avialable standard methods for function fitting
to one-dimesional distributions is the package RooFiLab (``Root 
Fits for Laboratory courses''). Based on ROOT, this program 
developed at KIT (Karlsruhe Institute of Technology, 
URL \href{http://www-ekp.physik.uni-Karlsruhe.de/$\sim$quast/RooFiLab}
{http://www-ekp.physik.uni-Karlsruhe.de/~quast/RooFiLab})
offers an
easy-to-use, structured graphical user interface and an  
ASCII input format for typical use cases in student laboratory 
courses. Correlated erros on both the x- and y-coordinate
are also supported. In the most general case, covariance 
matrices of the x- and y-coordinates
can be specified. There is also a simplified possibility for
special cases of fully correlated absolute or relative errors 
on the measurements.  
An example fit is shown in Figure\,\ref{fig:RooFiLabGraph}.
\begin{figure}[hbt] \begin{center}
\includegraphics[width=0.5\textwidth] {figures/RooFiLabGraph}
\caption[RooFiLab Example ] {\label{fig:RooFiLabGraph} 
 Example of a straight-line fit with independent and correlated 
 (systematic) errors on both the x- and y-directions. }
\end{center}\end{figure}

High flexibility in the definition of the model is achieved by
direct usage of the ROOT interpreter, which has been extended 
to use named parameters instead of parameter numbers. In addition,
more complex models can be implemented as C or C++ functions, 
wich are compiled and linked at run-time.

The elements of the grafical user interface 
(see Figure\,\ref{fig:Shutter2}) and control via the 
input file are described in the manual 
(file \verb\RooFiLab.pdf\ in the subdirectory \verb+RooFiLab/doc+,
in German language).  A brief overview is given here.

\subsection{Installation}  
\textit{RooFiLab} is availalbe, fully installed along with ROOT 
in a virtual 
machine\footnote{\url{http://www-ekp.physik.uni-karlsruhe.de/~quast/VMroot}} 
based on the Ubuntu distribution. The compressed disk image
is most easily imported into the freely available virtualisation tool 
\textit{VirtualBox} for the most common Linux distributions, for
Windows versions XP and later and for Macintosh operating systems.

The program code of \textit {RooFiLab} is distributed from
the URL given above as a compressed archive \verb+RooFiLab.tar.gz+. 
After unpacking, the installation under Linux proceeds by executing
 \verb+make+; the file \verb+Makefile+ contains all neccessary
instructions. A ROOT installation must be present and initialized, 
i.e. the environment variable \verb+PATH+ must contain the path
to the ROOT executable and \verb+LD_LIBRARY_PATH+ must point to
the ROOT libraries.

\subsection{Usage of \textit{RooFiLab} }

\textit{RooFiLab} offers two windows: one is used for 
control, the other is for graphics output. The control window,
as depicted in Figure\ref{fig:Shutter2},
is separated into four Shutters, offering the following actions
\begin{itemize}
\item data input and definition of functions and parameters
\item fixing of start values and  ``Fit-by-Eye''
\item execution of the fit, eventually iteratively by fixing 
  some of the free parameters
\item options for graphical output 
\end{itemize}\vspace{-0.5pc}

\begin{figure}[hbt] \begin{center}
\includegraphics[width=0.5\textwidth] {figures/RooFiLab_shutter2}
\caption[Graphical Interfacd of RooFiLab] {\label{fig:Shutter2} 
  The grafical user interface of RooFiLab.}
\end{center} \end{figure}

During execution, ROOT functionality is also available. Of 
particular importance are procedures for interactive 
manilulations of the output graphcis and their export. 
As usual, the context menu is opened by right-klicking 
of the components of the graph or via the Toolbar at the 
top of the graphics window.

In addition to interactive usage of the controls of the 
graphical interface, fits can also be executed automatically
by specification of control options in the input file definig
the data inputs. After an interactive fit,
options can thus be archived in the input file and then be
used for repeated, automated fits.

\section{Examples with \textit {RooFiLab}} \label{sec_examples}
The following subsections show simple examples illustrating the
usage of \textit{RooFiLab} and may serve as the basis for own
applications.

\subsection{Straight-line fit with correlated erros  in x and y}

This \textit {RooFiLab} input file contains several control lines
and documents the available options.
Control lines are comment lines starting with \verb+#!+ followed
by a keyword. The control command \verb+#! dofit = true+ triggers
an automated fit defined by the input data and the control options
in the file.

\begin{verbatim}
# straight-line fit to data with errors in x and y, incl. simple correlations
# ===========================================================================
#! staterrors = xy
#! systerrors = 0.02 0.04 rel rel

#! fit = "m*x+b" "m,b" "roofilab.fit"
#! initialvalues = 0.015 0

### command to execute fit
#! dofit = true
### show systematic erros as second error bar
#! secondgraph = syst

#! title = "Fit to data with correlated errors"
#! graphlegend = "Data" bottom right
#! functionlegend = "Model" bottom right
#! xaxis = "X-values"
#! yaxis = "Y-values or f(x)"

#! markersettings = 1.5 4 24
#! functionsettings = 1 3 2
#! grid = y
#! logscale = 0
#! savegraphic = "roofilab.eps"

# =================Eingabe der Daten ===================================
# values in up to four columns separated by whitespaces 
#       (except for linebreaks or linefeeds)
# x       y      ex      ey
4.05	0.035	0.12	0.006
4.36	0.056	0.13	0.007
4.68	0.052	0.09	0.005
4.80	0.044	0.09	0.005
5.09	0.048	0.14	0.007
5.46	0.055	0.14	0.007
5.71	0.066	0.17	0.009
5.83	0.048	0.21	0.011
6.44	0.075	0.22	0.011
8.09	0.070	0.28	0.014
8.72	0.097	0.32	0.016
9.36	0.080	0.37	0.018
9.60	0.120	0.39	0.020
\end{verbatim}

\clearpage \newpage 
\subsection{Averaging correlated measurements}

Averaging correlated measurements formally corresponds
to a fit of a constant. The measurements in this
example are the individual measurements of the
mass of the Z Boson at the electron-positron
collider LEP at CERN. The common error of
1.7\,MeV results from uncertainties in the 
centre-of-mass energy of the accelerator. The
line
\verb+ #! systerrors = 0 0.0017 abs abs +
specifies this common aboslute error on each 
measurement.
~\\

\begin{verbatim}
# Mesurements of Z-Mass by AELPH, DELPHI, L3 and OPAL
# ---------------------------------------------------

# graphics options 
#! markersettings = 1.5 4 24	
#! functionsettings = 1 3 3	
#! grid = y	
# logscale = 0	
# savegraphic = "roofilab.eps"	
# saverfl = "data.rfl"	

# plot lables
#! title = "averaging measurements"	
#! xaxis = "n"	
#! yaxis = "Mass of Z boson"	
#! graphlegend = "Z mass measurements" bottom right
#! functionlegend = "average Z mass" bottom right

# fit control
#! fit = "m" "m" "average.fit"	
#! initialvalues =  91.2	
#! dofit = true	

#! staterrors = y	# control-command
#! systerrors = 0 0.0017 abs abs
# the data, LEP electroweak working group, CERN 2000    
1 91.1893 0.0031
2 91.1863 0.0028
3 91.1894 0.0030
4 91.1853 0.0029
\end{verbatim}

\clearpage \newpage 
\subsection{Fit of a polynomyal to data with  Poisson errors}
This example show the fit of a fourth-order polynomial to
data with uncorrelated, Poissonian errors, i.\,e. erros
given by the square root of the data points. Although
the errors are non-Gaussion in this case,
a $\chi^2$-fit often results in acceptable results. 
With the option 
\verb+#! fitmethod = likelihood+ 
a likelihood method can be selected. In this case, the
statistical errors are ignored and may be ommitted.
For technical reasons, the x-values must be 
equi-distant in this case (due to usage of ROOT-class
\verb|TH1|). 

\begin{verbatim}
##########################################################
#     example: fit of an angular distribution
##########################################################

# plot commands
#! title = "angular distribution "
#! xaxis = "cos(theta)"
#! yaxis = "number of events"
#! graphlegend ="observed rate " top left 
#! functionlegend ="fitted cos(theta) distribution " top left 
#! markersettings = 1.5 2 5
#! functionsettings = 1 3 3    
# fit control
#! fit = "a4*x^4+a3*x^3+a2*x^2+a1*x+a0" "a0,a1,a2,a3,a4" "v_vs_cost.fit"
#! dofit = true

# fitmethod = likelihood # uncomment to perform a Log Likelihood fit

# definition of data
#! staterrors = y
# cost   N    sqrt(N)
-0.9    81.    9.0
-0.7    50.    7.1
-0.5    35.    5.9
-0.3    27.    5.2
-0.1    26.    5.1
 0.1    60.    7.7
 0.3   106.    10.3
 0.5   189.    13.7
 0.7   318.    17.8
 0.9   520.    22.8
\end{verbatim}

\clearpage \newpage 
\subsection{Correlated measurements with full covariance matrix}
As a more complex example the averaging procedure for
measurements of the W Boson mass is shown here.
Measurements of the four LEP experiments in two final states
have different systematic errors, which are correlated 
among groups of measurements. These are specified in 
the full 8$\times8$ covariance matrix, which is composed
of 4$\times4$ block matrices. The control line  
\verb+ #! covmatrices = 0 wmass.cov +. specifies that 
not covariance matrix in $x$ and the matrix \verb+wmass.cov+
are to be used in the fit.
 
\begin{verbatim}
# Mesurements of W-Mass by AELPH, DELPHI, L3 and OPAL
# ---------------------------------------------------
# ### example of fit with covariance matrix#
# --- graphics options 
#! markersettings = 1.5 4 24	
#! functionsettings = 1 3 3	
#! grid = y	
#! title = "averaging measurements"	
#! xaxis = "n"	
#! yaxis = "Mass of W boson"	
#! graphlegend = "W mass measurements" top right	
#! functionlegend = "average W mass" top right	
# --- fit control
#! fit = "m" "m" "Wmittelung.fit"	
#! initialvalues =  80.5	
#! dofit = true	
# --- the data (LEP electroweak working group, CERN 2006)
#! staterrors = 0
#! systerrors = 0 0 abs abs
#! covmatrices = 0 wmass.cov
1 80.429  0.059 # qqlv ALEPH
2 80.340  0.076 # qqlv DELPHI
3 80.213  0.071 # qqlv L3
4 80.449  0.062 # qqlv OPAL
5 80.475  0.082 # qqqq ALEPH
6 80.310  0.102 # qqqq DELPHI
7 80.323  0.091 # qqqq L3
8 80.353  0.081 # qqqq OPAL

//file wmass.cov
  0.003481 0.000316 0.000316 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.005776 0.000316 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.000316 0.005041 0.000316 0.000383 0.000383 0.000383 0.000383
  0.000316 0.000316 0.000316 0.003844 0.000383 0.000383 0.000383 0.000383
  0.000383 0.000383 0.000383 0.000383 0.006724 0.001741 0.001741 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.010404 0.001741 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.001741 0.008281 0.001741
  0.000383 0.000383 0.000383 0.000383 0.001741 0.001741 0.001741 0.006561
\end{verbatim}
