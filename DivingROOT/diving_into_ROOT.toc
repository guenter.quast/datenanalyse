\contentsline {chapter}{\numberline {1}Motivation and Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Welcome to ROOT}{4}{section.1.1}
\contentsline {chapter}{\numberline {2}ROOT Basics}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}ROOT as calculator}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}ROOT as Function Plotter}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Controlling ROOT}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}Plotting Measurements}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Histograms in ROOT}{8}{section.2.5}
\contentsline {section}{\numberline {2.6}Interactive ROOT}{9}{section.2.6}
\contentsline {section}{\numberline {2.7}ROOT Beginners' FAQ}{10}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}ROOT type declarations for basic data types}{10}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Configure ROOT at start-up}{10}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}ROOT command history}{11}{subsection.2.7.3}
\contentsline {subsection}{\numberline {2.7.4}ROOT Global Variables}{11}{subsection.2.7.4}
\contentsline {chapter}{\numberline {3}ROOT Macros}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}General Remarks on ROOT macros}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}A more complete example}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Summary of Visual effects}{16}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Colours and Graph Markers}{16}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Arrows and Lines}{16}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Text}{17}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Interpretation and Compilation}{17}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Compile a Macro with ACLiC}{17}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Compile a Macro with g++}{17}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Graphs}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Read Graph Points from File}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Polar Graphs}{20}{section.4.2}
\contentsline {section}{\numberline {4.3}2D Graphs}{21}{section.4.3}
\contentsline {chapter}{\numberline {5}Histograms}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}Your First Histogram}{23}{section.5.1}
\contentsline {section}{\numberline {5.2}Add and Divide Histograms}{24}{section.5.2}
\contentsline {section}{\numberline {5.3}Two-dimensional Histograms}{26}{section.5.3}
\contentsline {chapter}{\numberline {6}File I/O}{29}{chapter.6}
\contentsline {section}{\numberline {6.1}Storing ROOT Objects}{29}{section.6.1}
\contentsline {section}{\numberline {6.2}N-tuples in ROOT}{30}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Storing simple N-tuples}{30}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Reading N-tuples}{31}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Storing Arbitrary N-tuples}{32}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Processing N-tuples Spanning over Several Files}{33}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}{\em For the advanced user:} Processing trees with a selector script}{33}{subsection.6.2.5}
\contentsline {subsection}{\numberline {6.2.6}{\em For power-users:} Multi-core processing with \texttt {PROOF lite}}{36}{subsection.6.2.6}
\contentsline {subsection}{\numberline {6.2.7}Optimisation Regarding N-tuples}{37}{subsection.6.2.7}
\contentsline {chapter}{\numberline {7}Functions and Parameter Estimation}{39}{chapter.7}
\contentsline {section}{\numberline {7.1}Fitting Functions to Pseudo Data}{39}{section.7.1}
\contentsline {section}{\numberline {7.2}Toy Monte Carlo Experiments}{41}{section.7.2}
\contentsline {section}{\numberline {7.3}Fitting in General}{43}{section.7.3}
\contentsline {chapter}{\numberline {8}ROOT in \textsc {Python}\xspace }{47}{chapter.8}
\contentsline {section}{\numberline {8.1}PyROOT}{47}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}\bf More \textsc {Python}\xspace - less ROOT}{49}{subsection.8.1.1}
\contentsline {chapter}{\numberline {A}RooFiLab}{55}{appendix.A}
\contentsline {section}{\numberline {A.1}Root-based tool for fitting: RooFiLab}{55}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Installation}{56}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}Usage of \textit {RooFiLab}}{56}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}Examples with \textit {RooFiLab}}{56}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Straight-line fit with correlated erros in x and y}{57}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Averaging correlated measurements}{58}{subsection.A.2.2}
\contentsline {subsection}{\numberline {A.2.3}Fit of a polynomyal to data with Poisson errors}{59}{subsection.A.2.3}
\contentsline {subsection}{\numberline {A.2.4}Correlated measurements with full covariance matrix}{60}{subsection.A.2.4}
\contentsline {chapter}{\numberline {B}Markers, Colours, Symbols}{61}{appendix.B}
\contentsline {section}{\numberline {B.1}Colour Wheel and Graph Markers}{61}{section.B.1}
\contentsline {section}{\numberline {B.2}Lines and Arrows}{62}{section.B.2}
\contentsline {section}{\numberline {B.3}Latex Symbols}{62}{section.B.3}
\contentsline {chapter}{\numberline {C}Most Relevant Classes and their Methods}{63}{appendix.C}
