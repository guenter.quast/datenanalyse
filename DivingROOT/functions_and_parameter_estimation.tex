\chapter{Functions and Parameter Estimation}
After going through the previous chapters, you already know how 
to use mathematical functions (class \verb#TF1#), 
and you got some insight into
the graph (\verb#TGraphErrors#) and 
histogram classes (\verb#TH1F#) for data visualisation. 
In this chapter we will add more detail to 
the previous approximate explanations to face the 
fundamental topic of parameter estimation by fitting 
functions to data. For graphs and histograms, ROOT offers
an easy-to-use interface to perform fits - either the fit panel
of the graphical interface, or the \verb#Fit# method. 
The class \verb#TVirtualFitter# allows access to the detailed
results, and can also be used for more general tasks with
user-defined minimisation functions.

Very often it is necessary to study the statistical properties of 
analysis procedures. This is most easily achieved by applying the
analysis to many sets of simulated data (or ``pseudo data''),
each representing one possible version of the true experiment. 
If the simulation only deals with the final distributions
observed in data, and does not perform a full simulation
of the underlying physics and the experimental apparatus, 
the name ``Toy Monte Carlo'' is frequently used\footnote{ 
``Monte Carlo'' simulation means that random numbers play a
role here which is as crucial as in games of pure chance 
in the Casino of Monte Carlo.}.
Since the true values of all parameters are known in the pseudo-data, 
the differences between the parameter estimates from the analysis procedure
w.\,r.\,t. the true values can be determined, and it is also possible to
check that the analysis procedure provides correct error estimates.


\section{Fitting Functions to Pseudo Data}
In the example below, a pseudo-data set is produced and a 
model fitted to it. 

ROOT offers various fit methods, all inheriting from a virtual class
\verb#TVirtualFitter#. The default fitter in ROOT is MINUIT, a 
classical fitting package originally implemented in the 
FORTRAN programming language. Recently, a C++ version, MINUIT2, has
been added, and the new package FUMILI. All of these methods determine
the best-fit parameters, their errors and correlations by minimising
a $\chi^2$ or a negative log-likelihood function.
A pointer to the active fitting method is accessible via an instance 
of class \verb#TVirtualFitter#. Methods of this class allow to set
initial values or allowed ranges for the fit parameters, provide means for
fixing and releasing of parameters and offer steering options 
for the numerical precision, and - most importantly - allow to retrieve
the status of the fit upon completion and the fit results. 
The documentation of the class \verb#TVirtualFitter# gives a list of
all currently implemented methods. 

The complexity level of the code below is intentionally 
a little higher than in the previous examples. The graphical output
of the macro is shown in Figure~\ref{Fig:functions}:
%
\lstinputlisting{macros/macro8.cxx}  
\vspace{.5pc} \hfill\small file:\texttt{macro8.cxx}\\
\begin{itemize}
 \item Line 3-6: A simple function to ease the make-up of lines.
Remember that the class \verb|TF1| inherits from \verb|TAttLine|.
 \item Line 8-10: Definition of a customised function, namely a Gaussian 
(the ``signal'') plus a parabolic function, the ``background''.
\item Line 13-18: Some maquillage for the Canvas. In particular we 
want that the parameters of the fit appear very clearly and nicely on the plot.
\item Line 26-31: define and initialise an instance of \verb#TF1#.
\item Line 33-40: define and fill a histogram.
\item Line 42-48: for convenience, the same function as for the generation of
the pseudo-data is used in the fit; hence, we need to reset the function
 parameters. This part of the code is very important for each fit procedure,
 as it sets the initial values of the fit. 
\item Line 51: A very simple command, well known by now: fit the function to 
  the histogram. 
\item Line 53--58: retrieve the output from the fit  
Here, we simply print the fit result and access and print the 
covariance matrix of the parameters.
\item Line 60--end: plot the pseudo-data, the fitted function and the
 signal and background components at the best-fit values.
\end{itemize}
% 
\begin{figure}[htb]
\begin{center}
\includegraphics[width=.75\linewidth]{figures/functions}%
\end{center}
\caption[Function fit to pseudo data]{\label{Fig:functions}
   Function fit to pseudo-data}
\end{figure}
%


\section{Toy Monte Carlo Experiments}
Let us look at a simple example of a toy experiment comparing 
two methods to fit a function to a
histogram, the $\chi^2$ method and a method called ``binned 
log-likelihood fit'', both available in ROOT.  

As a very simple yet powerful quantity to check the quality of the 
fit results, we construct for each pseudo-data set the
so-called ``pull'', the difference of the estimated and the true 
value of a parameter, normalised to the estimated error 
on the parameter, $(p_{estim} - p_{true})/\sigma_p$.  
If everything is OK, the distribution of the pull values is a 
standard normal distribution, i.\,e. a Gaussian distribution
centred around zero with a standard deviation of one.

The macro performs a rather big number of toy experiments, 
where a histogram is repeatedly filled with Gaussian distributed 
numbers, representing the pseudo-data in this example. Each time, 
a fit is performed according to the selected method, and the 
pull is calculated and filled into a histogram. Here is the code:
%
\lstinputlisting{macros/macro9.cxx}
\vspace{-.5pc} \hfill\small file: \texttt{macro9.cxx}\\~\\
%
Your present knowledge of ROOT should be enough to understand all the 
technicalities behind the macro. Note that 
the variable \verb#pull# in line $54$ is different from the
definition above: instead of the parameter error on \verb#mean#,
the fitted standard deviation of the distribution divided by the
square root of the number of entries, \verb#sig/sqrt(n_tot_entries)#,
is used.
\begin{itemize}
 \item What method exhibits the better performance with the default 
parameters?
 \item What happens if you increase the number of entries per histogram 
by a factor of ten? Why?
\end{itemize}

\section{Fitting in General}
In the examples above, we used the simplified fitting interface 
of ROOT, and the default minimisation functions. 
In general, however,
fitting tasks often require special, user-defined minimisation 
functions. This is the case when data cannot be represented as 
one- or two-dimensional histograms or graphs, when errors 
are correlated and covariance matrices must be taken into account,
or when external constrains on some of the fit parameters exist. 
The default minimiser in ROOT is MINUIT, a package that has been
in use since decades. It offers several minimisation methods and
a large number of features accessible through the class \verb#TMinuit#.
A more modern, generalised interface allowing to use other minimises 
also exists (see class \verb#TVirtualFitter#), but still lacks some 
of the original features offered by \verb#TMinuit#. 

The macro below provides a rather general example.
Data is read from 
a file, stored in an n-tuple for repeated access, and an extended negative 
log-likelihood function is calculated and minimised with respect to
the fit parameters. 
%
\lstinputlisting{macros/negLogLfit.cxx}
\vspace{-.5pc} \hfill\small file: \texttt{negLogLfit.cxx}\\~\\
%
You already know most of the code fragments used above. The new 
part is the user-defined minimisation function \verb#fFCN#, made
known to the minimiser via the method \verb#SetFCN(void *f)#. 
\begin{itemize}
\item Lines 11--29: definition of function to be minimised; the parameter 
list (number of parameters, eventually analytically calculated derivatives
w.r.t. the parameters, the return value of the function, the array of
parameters, and a control flag) is fixed, as it is expected by the 
minimisation package. This function is repeatedly called by the minimisation
package with different values of the function parameters. 
\item Lines 35--63: initialisation of the fit: definition of a probability
density function as a \verb#TF1#, creation and filling of an n-tuple 
containing the data read from a file, and the definition of the 
fit parameters and their initial values and ranges. The minimiser
is instantiated in lines 48 and 49. 
\item Lines 66--68 execute the fit, first a general minimisation, and
 then an error analysis using the \verb#MINOS# method.
\item Lines 70--106:  retrieval of fit results after completion of the fit; 
this part needs access to the data and serves for a comparison of the 
fit result with the data - here, we show the fitted function on top of a 
histogram of the input data. Note that the PDF of a likelihood fit needs 
to be scaled to take into account the bin width of the histogram.  

\item Line 63--91: The function \verb#printFit# illustrates how to 
access the best-fit values of the parameters and their errors and
correlations from an object of \verb#TMinuit#. Here, they are 
written to standard output; it is easy to redirect this into a file or 
some other data structure, if required.
\item Code starting at line 93 illustrates how contour lines of two 
  fit parameters of one and two $\sigma$ are produced. The correlation 
  of the two variables \verb#tau# and \verb#off-set# is clearly visible
  (Figure~\ref{Fig:extLogLfit}). 
\end{itemize}
\begin{figure}[htb]
\begin{center} \begin{tabular}{lr}
\includegraphics[width=.49\linewidth]{figures/extLogLfit} &
\includegraphics[width=.49\linewidth]{figures/extLogLfitContours} \\
\end{tabular} \end{center}
\caption[Extended log liklihood fit ]{\label{Fig:extLogLfit}
  Histogrammed input data with overlayed scaled fit function, 
   and one- and 2-$\sigma$ contour lines from the likelihood fit.}
\end{figure}


\vfill

