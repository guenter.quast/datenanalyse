<!--
This is the source for the kafe2go documentation,
generated via   pandoc kafe2go.md -o index.html
-->

<html lang="de">
  <head>
    <meta http-equiv="Content-Type" content="text/html;
      charset=UTF-8">
    <meta name="Author" content="G&uuml;nter Quast">
    <meta name="Description" content="Datenauswertung mit kafe2go" lang="de">
    <meta name="created" content="2022-05-23">
    <meta name="Language" content="german">
    <meta name="keywords" content="Datenauswertung, Datenanalyse, physikalisches Praktikum, kafe2">
    <meta name="robots" content="index, follow">
    <title>Modellanpassung mit kafe2go</title>
    
  <style>
     body {
          margin-left:20px;
	  text-align:justify;
	  max-width:55em;
          font-family:Helvetica, Sans-Serif;
          color:black;
          background-color:White;
     }
  </style>
</head>
    
<body style="background-color:white;">

<h1 ALIGN="center"; style="color:RoyalBlue">
    Anpassung von Modellen an Messdaten mit kafe2go
</h1>

Das Paket [*kafe2*](https://github.com/dsavoiu/kafe2) 
zur Anpassung von Modellen an Messdaten 
enthält das eigenständige Skript
*kafe2go.py*, mit dem Anpassungen ganz ohne die Notwenigkeit der
Erstellung von eigenem Python-Code durchgeführt werden können. 
Daten und Modellfunktion werden dazu in einer Datei im Format der
einfachen Datenbeschreibungssprache *yaml* eingetregen, wie unten
im Beispiel gezeigt. Der Aufruf erfolgt mittels

`> kafe2go.py kafe2go_fit.yml`

auf der Kommandozeile. 

Wenn auf dem Rechner *Python* und das Paket *kafe2* mittels
`pip3 install kafe2` installiert sind, dann ist *kafe2go.py*
als Skript im Verzeichnis der ausführbaren Dateien verfügbar.

*Anmerkung*: Für das Betriebssystem MS Windows müssen noch Dateien
vom Typ *.py* mit der Anwendung *Python.exe* assoziiert werden.
Wichtige weitere Hinweise finden sich im Skript "Datenauswertung"
im Anhang A: "Tipps für MS Windows".

Die ausführliche 
[Dokumentation des *kafe2*-Pakets](https://kafe2.readthedocs.io/en/latest/)
enthält auch einen Abschnitt zur Anwendung von 
[*kafe2go*](https://kafe2.readthedocs.io/en/latest/parts/user_guide_kafe2go.html) 
mit der Beschreibung der entsprechenden Schlüsselwörter zur Dateneingabe und
zur Steuerung der Anpassung.

Die Möglichkeiten sind im Vergleich zur Verwendung von *Python*-Code etwas
eingeschränkter, dafür ist die Durchführung von Anpassungen aber sehr einfach. 

<!--
Für das Betriebssystem MS-Windows gibt es eine 
[ausführbare Datei](http://etpwww.etp.kit.edu/~quast/kafe2/kafe2go/kafe2go.zip),
die Anpassungen mit Hilfe eines Rechtsklicks auf eine Datei im *yaml*-Format
und anschließendem Öffnen mit der Anwendung *kafe2go.exe* ermöglicht.
-->

Die *yaml*-Datei für die Anpassung einer Parabel sieht folgendermassen aus:

```
# kafe2go_fit.yml: example of a fit with kafe2go
# -----------------------------------------------
#  execute via:
#    kafe2go kafe2go_fit.yml

# In this example a 2nd order polynomial
# is fit to data points with uncertainties.

# -----------------------------------------------

label: 'Beispieldaten'

x_data: [.05,0.36,0.68,0.80,1.09,1.46,1.71,1.83,2.44,2.09,3.72,4.36,4.60]
x_errors: 5%
x_label: 'x'

y_data: [0.35,0.26,0.52,0.44,0.48,0.55,0.66,0.48,0.75,0.70,0.75,0.80,0.90]
y_errors: [.06,.07,.05,.05,.07,.07,.09,.1,.11,.1,.11,.12,.1]
y_label: 'data & f(x)'

model_label: 'Parabolel-Modell'
model_function: |
  def poly2(x, a, b, c ):
      # Our first model is a simple linear function
      return a * x*x + b*x + c
``` 

Alle [Dateien](http://etpwww.etp.kit.edu/~quast/kafe2/kafe2go/)
