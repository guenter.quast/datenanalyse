# Datenanalyse

## Sammlung von Skripten und Materialien zur Datenanalyse  
>                                          Günter Quast, g.quast@kit.edu
                                        
- Übersichtsdarstellung "Datenauswertung"
- Skript "Chi2 Methode"
- Beschreibung zur virtuellen Maschine für Datenanalyse und Kurse am ETP
- jupyter Notebooks mit Tutorien zur Datenauswertung - siehe Einführung im
   [Wiki](https://git.scc.kit.edu/yh5078/datenanalyse/-/wikis/jupyter-Tutorial).
- Skript "Diving into Root" und root-Macros

Zum Ausprobieren der Jupyter-Tutorials: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.scc.kit.edu%2Fyh5078%2Fdatenanalyse/master)
